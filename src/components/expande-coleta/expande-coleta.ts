import { Component, ViewChild, Input, ElementRef, Renderer } from '@angular/core';


@Component({
  selector: 'expande-coleta',
  templateUrl: 'expande-coleta.html'
})
export class ExpandeColetaComponent {
  
  @ViewChild('expandWrapper', {read: ElementRef}) expandWrapper;
  @Input('expanded') expanded;
  @Input('expandHeight') expandHeight;

  constructor(public renderer: Renderer) {

  }

  ngAfterViewInit(){
    this.renderer.setElementStyle(this.expandWrapper.nativeElement, 'height', this.expandHeight + 'px');
  }
}
