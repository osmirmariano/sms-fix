import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

@Component({
  selector: 'popover',
  templateUrl: 'popover.html'
})
export class PopoverComponent {
  itens = [];

  constructor(public viewCtrl: ViewController) {
    this.itens = [
      {icon: 'eye', item: 'Visualizar'},
      {icon: 'checkbox', item: 'Coleta de dados'},
      {icon: 'pie', item: 'Gráfico'},
      {icon: 'calendar', item: 'Plano de ação'},
      {icon: 'trash', item: 'Excluir'},
      {icon: 'checkmark-circle', item: 'Finalizar'}
      // {icon: 'create', item: 'Editar'},
      // {icon: 'list-box', item: 'Dados coletados'}
    ]
  }

  itemClick(item){
    this.viewCtrl.dismiss(item);
  }
}
