import { Component, ViewChild, Input, ElementRef, Renderer } from '@angular/core';

@Component({
  selector: 'expande-item',
  templateUrl: 'expande-item.html'
})

export class ExpandeItemComponent {
  @ViewChild('expandWrapper', {read: ElementRef}) expandWrapper;
  @Input('expanded') expanded;
  @Input('expandHeight') expandHeight;

  constructor(public renderer: Renderer) {

  }

  ngAfterViewInit(){
    this.renderer.setElementStyle(this.expandWrapper.nativeElement, 'height', this.expandHeight + 'px');
  }
}
