import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';


@Component({
  selector: 'popover-nova',
  templateUrl: 'popover-nova.html'
})
export class PopoverNovaComponent {
  itens = [];

  constructor(public viewCtrl: ViewController) {
    this.itens = [
      {icon: 'eye', item: 'Visualizar'},
      {icon: 'checkbox', item: 'Coleta de dados'},
      {icon: 'trash', item: 'Excluir'}
    ]
  }

  itemClick(item){
    this.viewCtrl.dismiss(item);
  }
}
