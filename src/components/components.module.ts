import { NgModule } from '@angular/core';
import { PopoverComponent } from './popover/popover';
import { ExpandeItemComponent } from './expande-item/expande-item';
import { ExpandeAuditoriaComponent } from './expande-auditoria/expande-auditoria';
import { ExpandeAndamentoComponent } from './expande-andamento/expande-andamento';
import { ExpandeFinalizadaComponent } from './expande-finalizada/expande-finalizada';
import { ExpandeAuditoriaNormasComponent } from './expande-auditoria-normas/expande-auditoria-normas';
import { ExpandeAuditoriaSubnormasComponent } from './expande-auditoria-subnormas/expande-auditoria-subnormas';
import { ExpandeColetaComponent } from './expande-coleta/expande-coleta';
import { PopoverNovaComponent } from './popover-nova/popover-nova';
@NgModule({
	declarations: [
        PopoverComponent,
        ExpandeItemComponent,
        ExpandeAuditoriaComponent,
        ExpandeAndamentoComponent,
        ExpandeFinalizadaComponent,
        ExpandeAuditoriaNormasComponent,
        ExpandeAuditoriaSubnormasComponent,
        ExpandeColetaComponent,
        PopoverNovaComponent
    ],
	imports: [
        
    ],
    exports: [
        PopoverComponent,
        ExpandeItemComponent,
        ExpandeAuditoriaComponent,
        ExpandeAndamentoComponent,
        ExpandeFinalizadaComponent,
        ExpandeAuditoriaNormasComponent,
        ExpandeAuditoriaSubnormasComponent,
        ExpandeColetaComponent,
        PopoverNovaComponent
    ]
})
export class ComponentsModule {}
