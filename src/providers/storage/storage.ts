
import { Injectable } from '@angular/core';
import { ServiceProvider } from '../service/service';
import { Storage } from '@ionic/storage';
import { LoadingProvider } from '../loading/loading';

@Injectable()
export class StorageProvider {
  private nr: any;
  private subnr: any;
  // public subnr: any = Array(); 

  constructor(
    private storage: Storage,
    private serviceProvider: ServiceProvider,
    private loadingProvider: LoadingProvider) {

  }
  
  ionViewWillEnter(){
  }

  /**
   * MÉTODO PARA RETORNAR O TAMANHO DO BANCO DE DADOS OFFLINE
   */
  tamanho(){
    return this.storage.length();
  }

  carregamentoNR(){
    this.serviceProvider.carregamentoPorDemandaNR()
    .then(nr => {
      if (nr) {
        this.nr = nr;
        this.insertNR(this.nr);
      }
    })
    .catch(erro => {
      console.log('ERRO AO CARREGAR NRS: ', erro);
    });
  }

  // carregamentoSubnr(nr){
  //   nr.forEach(element => {
  //     this.serviceProvider.carregamentoPorDemandaSubnr(element.idNR)
  //       .then(sub => {
  //         if(sub){
  //           this.insertSubnr(sub, element.idNR);
  //           this.subnr = sub;
  //           this.carregamentoRequisito(this.subnr);
  //         }
  //       });
  //   });
  // }

  carregamentoRequisito(subnr){
    subnr.forEach(requisito => {
      this.serviceProvider.carregamentoPorDemandaResquisitos(requisito.idsubNR)
      .then(req => {
        if(req){
          console.log('RETORNO REQ: ', req);
          this.insertRequisto(req, requisito.idsubNR);
          this.loadingProvider.dismiss();
        }
      })
      .catch(erro => {
        this.serviceProvider.carregamentoPorDemandaSubnr(requisito.idsubNR)
        .then(certo => {
          this.insertRequisto(certo, requisito.idsubNR);
          this.loadingProvider.dismiss();
        });
        console.log('ERRO REQUISITOS: ', erro);
      });
    });
  }

  /**
   * MÉTODO PARA CARREGAR POR DEMANDAS AS NRS E SUBNRS E SETAR AMBAS NO BANCO DE DADOS OFFLINE
   */ 
  //remover DEPOIS
  carregar() {
    this.loadingProvider.loadBanco();
    this.serviceProvider.carregamentoPorDemandaNR()
      .then(nr => {
        if (nr) {
          this.nr = nr;
          this.insertNR(this.nr);
          this.nr.forEach(element => {
            this.serviceProvider.carregamentoPorDemandaSubnr(element.idNR)
            .then(sub => {
              if(sub){                
                this.insertSubnr(sub, element.idNR);
                this.subnr = sub;
                this.subnr.forEach(requisito => {
                  this.serviceProvider.carregamentoPorDemandaResquisitos(requisito.idsubNR)
                  .then(req => {
                    if(req){
                      console.log('RETORNO REQ: ', req);
                      this.insertRequisto(req, requisito.idsubNR);
                      this.loadingProvider.dismiss();
                    }
                  })
                  .catch(erro => {
                    setTimeout(
                      this.serviceProvider.carregamentoPorDemandaSubnr(requisito.idsubNR)
                      .then(certo => {
                        this.insertRequisto(certo, requisito.idsubNR);
                      }), 300);
                    console.log('ERRO REQUISITOS: ', erro);
                  });
                });
              }
            })
            .catch(erro => {
              setTimeout(
                this.serviceProvider.carregamentoPorDemandaSubnr(element.idNR)
                .then(certo => {
                  this.insertRequisto(certo, element.idNR);
                }), 300);
              console.log('ERRO SUBNR: ', erro);
            });
          });
        }
      })
      .catch(erro => {
        console.log('ERRO AO CARREGAR NRS')
      });
  }

  /*
  storageNRs(){
    this.serviceProvider.carregamentoPorDemandaNR()
    .then(nr => {
      if (nr) {
        this.nr = nr;
        this.insertNR(this.nr);
        this.storageSubnr();
      }
    })
    
  }

  storageSubnr(){
    this.nr.forEach(nr => {
      console.log('NRRR: ', nr.idNR);
      this.serviceProvider.carregamentoPorDemandaSubnr(nr.idNR)
      .then(subnr => {
        if(subnr){
          this.subnr = subnr;
          this.subnr.forEach(element => {
            this.insertSubnr(subnr, element.idsubNR);
          });
          this.storageRequisito()
        }
      })
    });
  }

  storageRequisito(){
    this.subnr.forEach(requisito => {
      this.serviceProvider.carregamentoPorDemandaResquisitos(requisito.idsubNR)
        .then(requisitos => {
          this.insertRequisto(requisitos, requisito.idsubNR);
          this.loadingProvider.dismiss();
        })
        .catch(erro => {
          console.log('ERRO REQUISITOS: ', erro);
        });
    });
  }
  */

  /**
   * MÉTODO PARA CARREGAR AS AUDITORIAS DA API
   */
  strongeAuditoria(){
    this.serviceProvider.auditoria()
    .then(auditoria => {
      if(auditoria){
        console.log('AUDITORIAS: ', auditoria);
        this.insertAuditoria(auditoria);
      }
    })
    .catch(erro => {
      console.log('DEU ERRO AUDITORIA: ', erro);
    })
  }

  /**
   * MÉTODO PARA CARREGAR AS OPÇÕES DAS PERGUNTAS DA API
   */
  storangeOpcao(){
    this.serviceProvider.perguntas()
    .then(opcao => {
      if(opcao){
        this.insertOpcao(opcao);
      }
    })
    .catch(erro => {
      console.log('DEU ERRO OPÇÃO: ', erro);
    })
  }

  insertNRSubnr(){
    this.loadingProvider.loadBanco();
    this.serviceProvider.carregamentoNrSubnr()
    .then(certo => {
      var dados = JSON.stringify(certo);
      const jsonDados = JSON.parse(dados).nr;
      console.log('DADOS: ', jsonDados);
      this.storage.set('nrsubnr', JSON.parse(dados))
      .then(certo => {
        jsonDados.forEach(nr => {
          console.log('NR: ', nr);
          nr.subNR.forEach(subnr => {
            console.log('SUBNR: ', subnr);
            this.serviceProvider.carregamentoPorDemandaResquisitos(subnr.idsubNR)
              .then(req => {
                if(req){
                  console.log('RETORNO REQ: ', req);
                  this.insertRequisto(req, subnr.idsubNR);
                  this.loadingProvider.dismiss();
                }
              })
          });
        });
      })
      .catch(erro => {
        console.log('DEU ERRADO nrsubnr: ', erro);
      })
      
    })
  }



  getNRSubnr(){
    this.loadingProvider.load();
    return new Promise((resolve, reject) => {
      this.storage.get('nrsubnr')
        .then(val => {
          this.loadingProvider.dismiss();
          console.log('STORAGE NR : ', val); 
          resolve(val);
        });
      });
  }

  /**
   * MÉTODO PARA INSERIR AS NRS NO BANCO DE DADOS OFFLINE. ELE RECEBE COMO PARÂMETRO AS NRS QUE VEM DA 
   * API DO SMSFIX
   * @param nr 
   */
  insertNR(nr) {
    this.storage.set('nr', nr)
      .then((certo) => {
        //this.carregamentoSubnr(certo);
        // console.log('DEU CERTO NR: ', certo);//DEPOIS ALTERAR PARA COLOCAR ALGO MELHOR
      })
      .catch((erro) => {
        console.log('DEU ERRO NR: ', erro);
      })
  }

  /**
   * MÉTODO PARA INSERIR AS SUBNRS NO BANCO DE DADOS OFFLINE
   * @param subnr 
   * @param idSubnr 
  */ 
  insertSubnr(subnr, idSubnr) {
    this.storage.set('subnr-'+idSubnr, subnr)
      .then((certo) => {
        console.log('DEU CERTO SUBNR: ', certo);
      })
      .catch((erro) => {
        console.log('DEU ERRO SUBNR: ', erro);
      });
  }

  /**
   * MÉTODO PARA INSERIR OS REQUISITOS DAS SUBNR NO BANCO OFFLINE
   * @param requisito 
   * @param idSubnr 
   */
  insertRequisto(requisito, idSubnr){
    this.storage.set('requisito-'+idSubnr, requisito)
    .then(certo => {
      // console.log('DEU CERTO REQUISITO: ', certo);
    })
    .catch(erro => {
      console.log('DEU ERRADO REQUISITO: ', erro);
    });
  }

  /**
   * MÉTODO PARA INSERIR AUDITORIAS NO BANCO DE DADOS OFFLINE
   * @param auditoria 
   */
  insertAuditoria(auditoria){
    this.storage.set('auditoria', auditoria)
    .then(certo => {
      console.log('DEU CERTO AUDITORIA: ', certo);
    })
    .catch(erro => {
      console.log('DEU ERRO AUDITORIA: ', erro);
    })
  }

  /**
   * MÉTODO PARA INSERIR OPCAO NO BANCO DE DADOS OFFLINE
   * @param opcao 
   */
  insertOpcao(opcao){
    this.storage.set('opcao', opcao)
    .then(certo => {
      console.log('DEU CERTO OPÇAO: ', certo);
    })
    .catch(erro => {
      console.log('DEU ERRO OPÇAO: ', erro);
    })
  }

  /**
   * MÉTODO PARA PUXAR TODAS AS NRS QUE ESTÃO DENTRO DO BANCO DE DADOS OFFLINE
   */
  getTodasNr(){
    return new Promise((resolve, reject) => {
      this.storage.get('nr')
        .then(val => {
          console.log('STORAGE NR : ', val); 
          resolve(val);
        });
      });
  }

  /**
   * MÉTODO PARA PUXAR TODAS AS SUBNRS QUE ESTÃO DENTRO DO BANCO DE DADOS OFFLINE
   * @param numNr 
   */
  getTodasSubnr(numNr){
    this.loadingProvider.load();
    return new Promise((resolve, reject) => {
      this.storage.get('subnr-'+numNr)
        .then(val => {
          console.log('STORAGE SUBNR : ', val);
          this.loadingProvider.dismiss();
          resolve(val);
        });
    });
  }
  
  /**
   * MÉTODO PARA PUXAR TODOS OS REQUISITOS CORRESPONDENTE AO ID DA SUBNR
   * @param idSubnr 
   */
  getTodosRequisitos(idSubnr){
    this.loadingProvider.load();
    return new Promise((resolve, reject) => {
      this.storage.get('requisito-'+idSubnr)
      .then(val => {
        console.log('STORAGE REQUISITOS : ', val);
        this.loadingProvider.dismiss();
        resolve(val);
      });
    })
  }


  /**
   * MÉTODO PARA PUXAR TODAS AS AUDITORIAS QUE ESTÃO DENTRO DO BANCO DE DADOS OFFLINE
   */
  getTodasAuditoria(){
    return new Promise((resolve, reject) => {
      this.storage.get('auditoria')
        .then(val => {
          resolve(val);
          console.log('STORAGE AUDITORIA: ', val);
        });
    });
  }


  synchronize() {

  }
}
