import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';

@Injectable()

export class ServiceProvider {
  public loader: any;
  // api: any = 'http://192.168.1.115:8080/sms-fix/';//Teste
  api: any = 'http://www.smsfix.com.br/sms-fix/';//Produção


  constructor(public http: Http,
    public loadingCtrl: LoadingController) {
  }

  /**
   * MÉTODO PARA INICIAR O LOADING
   */
  load() {
    this.loader = this.loadingCtrl.create({
      content: ''
    });
    this.loader.present();
  }

  /**
   * MÉTODO PARA PARAR O CARREGAMENTO DO LOADING
   */
  dismiss() {
    if (this.loader) { this.loader.dismiss(); this.loader = null; }
  }

  /**
   * MÉTODO PARA FAZER LOGIN NO APLICATIVO
   * @param email 
   * @param senha 
   */
  login(email: string, senha: string) {
    let dados = {
      nome: email,
      senha: senha
    }

    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');

    let options = new RequestOptions({
      headers: headers
    });
    return new Promise((resolve, reject) => {
      this.http.post(this.api + 'Mobile/login', dados, options)
        .subscribe(res => {
          var nameUser = JSON.stringify(res);
          window.localStorage.setItem('nome', JSON.parse(nameUser)._body);
          resolve(res.json());
        },
          (err) => {
            reject(err);
          });
    })
  }

  /**
   * MÉTODO PARA CRIAR CONTA DE USUÁRIOS
   * @param nome 
   * @param email 
   * @param senha 
   * @param dd 
   * @param telefone 
   * @param termo_aceite 
   */
  criarConta(nome: any, email: any, senha: any, dd: any, telefone: any, termo_aceite: boolean) {
    let dados = {
      nome: nome,
      email: email,
      senha: senha,
      dd: dd,
      telefone: telefone,
      termo_aceite: termo_aceite,
    }
    console.log('DADOS ENVIO: ', dados);
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');

    let options = new RequestOptions({
      headers: headers
    });

    console.log('DADOS: ', dados);

    return new Promise((resolve, reject) => {
      this.http.post(this.api + 'Mobile/cadastroUsuario', dados, options)
        .subscribe(res => {
          var recebe = JSON.stringify(res);
          var final = JSON.parse(recebe)._body;
          resolve(final);
        },
          (err) => {
            var recebeComoJson = JSON.stringify(err);
            var teste = JSON.parse(recebeComoJson)._body;
            console.log('ERRO JSON: ', teste);
            reject('erro');
          });
    })
  }
 
  /**
   * MÉTODO PARA ENVIA CNPJ PARA API PARA FAZER REQUISIÇÃO VIA API SMS, MAS ESTÁ DEPRECIADO, REMOVER ELE
   * @param dadosReceita 
   */
  // cadastroCnpj(dadosReceita){
  //   let headers = new Headers();
  //   headers.append('Accept', 'application/json');

  //   let options = new RequestOptions({
  //     headers: headers
  //   });
    
  //   return new Promise((resolve, reject) => {
  //     this.http.post(this.api + 'Mobile/cnpj', dadosReceita, options)
  //       .subscribe(res => {
  //         var recebeComoString = JSON.stringify(res);
  //         let recebeComoJson = JSON.parse(recebeComoString)._body;
  //         resolve(recebeComoJson);
  //       },
  //       (err) => {
  //         this.dismiss();
  //         reject(JSON.parse(err))
  //       });
  //     })
  // }

  /**
   * MÉTODO PARA MOSTRAR AS AUDITORIAS
   */
  auditoria() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');

    let options = new RequestOptions({
      headers: headers
    });
    this.load();
    return new Promise((resolve, reject) => {
      this.http.get(this.api + 'Mobile/clientes', options)
        .subscribe(res => {
          var recebeComoString = JSON.stringify(res);
          let recebeComoJson = JSON.parse(recebeComoString);
          this.dismiss();
          resolve(JSON.parse(recebeComoJson._body));
        },
        (err) => {
          reject(JSON.parse(err))
        });
    })
  }

  /**
   * MÉTODO PARA ENVIAR AUDITORIA, PRIMEIRA ETAPA
   * @param valor 
   */
  enviarAuditoria(valor, empresa) {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');

    let options = new RequestOptions({
      headers: headers
    });

    let dados = {
      descricao: valor.descricao,
      tipo: valor.tipo,
      nomeAuditor: valor.nomeAuditor,
      cnpj: empresa.cnpj,
      empresa: empresa.nome,
      setor: valor.setor,
      numFuncionario: valor.numFuncionario,
      cnae: valor.cnae,
      registroMTE: valor.registroMTE,
      area: valor.area,
      grauRisco: valor.grauRisco,
      usuario: JSON.parse(window.localStorage.getItem('nome')).idusuario,
      dataCriacao: valor.dataCriacao,
      dataPrevista: valor.dataPrevista,
      nomeClientes: empresa.nome
    }
    this.load();
    return new Promise((resolve, reject) => {
      this.http.post(this.api + 'Mobile/auditoria', dados, options)
        .subscribe(res => {
          console.log('RES: ', res);
          window.localStorage.setItem('idAuditoria', JSON.parse(JSON.stringify(res))._body);
          this.dismiss();
          resolve(JSON.parse(JSON.stringify(res))._body);
        },
          (err) => {
            this.dismiss();
            reject(JSON.parse(err))
          });
    })
  }

  /**
   * MÉTODO PARA LISTAR TODOS OS USUÁRIOS DA PLATAFORMA
   */
  listarUsuarios() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');

    let options = new RequestOptions({
      headers: headers
    });

    return new Promise((resolve, reject) => {
      this.http.post(this.api + 'Mobile/usuarios', options)
        .subscribe(res => {
          var recebeComoString = JSON.stringify(res);
          let recebeComoJson = JSON.parse(recebeComoString);
          resolve(JSON.parse(recebeComoJson._body).usuario);
        },
        (err) => {
          reject(JSON.parse(err))
        });
    });
  }

  /**
   * MÉTODO PARA LISTAR AS PERGUNTAS DA COLETA DE DADOS DA AUDITORIA
   */
  perguntas() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');

    let options = new RequestOptions({
      headers: headers
    });

    return new Promise((resolve, reject) => {
      this.http.get(this.api + 'Mobile/opcoes', options)
        .subscribe(res => {
          var recebeComoString = JSON.stringify(res);
          let recebeComoJson = JSON.parse(recebeComoString);
          resolve(JSON.parse(recebeComoJson._body).opcoes);
        },
        (err) => {
          console.log('ERRO');
          reject(JSON.parse(err))
        });
    })
  }

  /**
   * MÉTODO PARA ENVIA OS DADOS DA COLETA DE DADOS DA AUDITORIA
   * @param dados 
   */
  enviaColeta(dados) {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');

    let options = new RequestOptions({
      headers: headers
    });
    this.load();
    return new Promise((resolve, reject) => {
      this.http.post(this.api + 'Mobile/coleta', JSON.stringify(dados), options)
        .subscribe(res => {
          console.log('RES: ', res);
          var recebeComoString = JSON.stringify(res);
          let recebeComoJson = JSON.parse(recebeComoString);
          this.dismiss();
          resolve(JSON.parse(recebeComoJson._body));
        },
        (err) => {
          this.dismiss();
          reject(JSON.parse(err))
        });
    })
  }

  /**
   * MÉTODO PARA FINALIZAR A COLETA DE DADOS
   * @param dados 
   * @param dadosParecer 
   */
  finalizarColeta(dados, dadosParecer) {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');

    let options = new RequestOptions({
      headers: headers
    });
    let parecer = {
      parecer: dadosParecer.parecer,
      critico: dadosParecer.critico
    }
    this.load();
    return new Promise((resolve, reject) => {
      this.http.post(this.api + 'Mobile/parecerTec/' + dados, JSON.stringify(parecer), options)
        .subscribe(res => {
          this.dismiss();
          resolve(res);
        },
        (err) => {
          this.dismiss();
          reject(JSON.parse(err))
        });
    })
  }

  /**
   * MÉTODO PARA ENVIAR OS ITENS DAS NRS SELECIONADOS
   * @param nrQuestoes 
   * @param idAuditoria 
   */
  enviaQuestoes(nrQuestoes: any, idAuditoria: any) {
    console.log('SELECIONADAS: ', nrQuestoes);
    console.log('ID: ', idAuditoria);
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');

    let options = new RequestOptions({
      headers: headers
    });
    this.load();
    return new Promise((resolve, reject) => {
      this.http.post(this.api + 'Mobile/auditoriaQuestoes/' + idAuditoria, JSON.stringify(nrQuestoes), options)
        .subscribe(res => {
          this.dismiss();
          resolve(res);
        },
        (err) => {
          this.dismiss();
          reject(JSON.parse(err))
        });
    })
  }

  /**
   * MÉTODO PARA CARREGAMENTO SOMENTE DAS NR - FAZ PARTE DO CARREGAMENTO LENTO
   */
  carregamentoPorDemandaNR() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');

    let options = new RequestOptions({
      headers: headers
    });
    // this.load();
    return new Promise((resolve, reject) => {
      this.http.post(this.api + 'Mobile/NR', options)
        .subscribe(res => {
          // this.dismiss();
          var recebeComoString = JSON.stringify(res);
          let recebeComoJson = JSON.parse(recebeComoString);
          resolve(JSON.parse(recebeComoJson._body));
        },
          (err) => {
            // this.dismiss();
            reject(err);
          });
    })
  }

  /**
   * MÉTODO PARA CARREGAR SOMENTE AS SUBNR DE ACORDO COM O NÚMERO DA NR - FAZ PARTE DO CARREGAMENTO 
   * POR DEMANDA
   * @param idNr 
   */
  carregamentoPorDemandaSubnr(idNr: any) {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');

    let options = new RequestOptions({
      headers: headers
    });
    // this.load();
    return new Promise((resolve, reject) => {
      this.http.post(this.api + 'Mobile/NRTeste/' + idNr, options)
        .subscribe(res => {
          // this.dismiss();
          var recebeComoString = JSON.stringify(res);
          let recebeComoJson = JSON.parse(recebeComoString);
          resolve(JSON.parse(recebeComoJson._body));
        },
        (err) => {
          reject(err.ok);
        });
    })
  }

  carregamentoPorDemandaSubnrTeste() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');

    let options = new RequestOptions({
      headers: headers
    });
    // this.load();
    return new Promise((resolve, reject) => {
      this.http.post(this.api + 'Mobile/subNrTeste/', options)
        .subscribe(res => {
          // this.dismiss();
          var recebeComoString = JSON.stringify(res);
          let recebeComoJson = JSON.parse(recebeComoString);
          resolve(JSON.parse(recebeComoJson._body));
        },
        (err) => {
          // this.dismiss();
          reject(JSON.parse(err))
        });
    })
  }

  /**
   * MÉTODO PARA CARREGAR POR DEMANDAS SOMENTE OS REQUISITOS
   * @param requisito 
   */
  carregamentoPorDemandaResquisitos(requisito){
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');

    let options = new RequestOptions({
      headers: headers
    });
    // this.load();
    return new Promise((resolve, reject) => {
      this.http.post(this.api + 'Mobile/subNR/' + requisito, options)
        .subscribe(res => {
          var recebeComoString = JSON.stringify(res);
          let recebeComoJson = JSON.parse(recebeComoString);
          resolve(JSON.parse(recebeComoJson._body));
        },
        (err) => {
          reject(err.ok)
        });
    });
  }
  
  /**
   * MÉTODO PARA PUXAR OS DADOS PARA GERAR O GRÁFICO DAS SUBNORMAS
   * @param grafico 
   */
  paraGrafico(grafico: any) {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');

    let options = new RequestOptions({
      headers: headers
    });

    return new Promise((resolve, reject) => {
      this.http.post(this.api + 'Mobile/reqgrafico/' + grafico, options)
        .subscribe(res => {
          let dados = res;
          console.log('DADOS: ', dados);
          const recebeComoString = JSON.stringify(dados)
          const recebeComoJson = JSON.parse(recebeComoString);
          resolve(JSON.parse(recebeComoJson._body));
        },
          (err) => {
            console.log('ERRO');
            reject(JSON.parse(err))
          });
    }
    )
  }

  /**
   * MÉTODO PARA LISTAR OS VALORES DA TABELA DE MULTAS PARA PODER REALIZAR A COMPOSIÇÃO DO CÁLCULO
   * PARA O PLANO DE AÇÃO
   */
  listarValores() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');

    let options = new RequestOptions({
      headers: headers
    });
    this.load();
    return new Promise((resolve, reject) => {
      this.http.get(this.api + 'Mobile/gradacaoMultas', options)
        .subscribe(res => {
          var recebeComoString = JSON.stringify(res);
          let recebeComoJson = JSON.parse(recebeComoString);
          this.dismiss();
          resolve(JSON.parse(recebeComoJson._body));
        },
          (err) => {
            this.dismiss();
            reject(JSON.parse(err))
          });
    })
  }

  /**
   * MÉTODO PARA FAZER REQUISIÇÃO NA API DA RECEITA FEDERAL PARA RETORNAR OS DADOS DA EMPRESA DE ACORDO 
   * COM O CNPJ
   * @param cnpj 
   */
  cnpj(cnpj) {
    console.log('CNPJ LIMPO: ', cnpj);
    var novoCnpj = cnpj.replace('.', '');
    novoCnpj = novoCnpj.replace('.', '');
    novoCnpj = novoCnpj.replace('/', '');
    novoCnpj = novoCnpj.replace('-', '');
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');

    let options = new RequestOptions({
      headers: headers
    });

    return new Promise((resolve, reject) => {
      this.http.get('https://www.receitaws.com.br/v1/cnpj/' + novoCnpj, options)
        .subscribe(res => {
          console.log('Resultado do CNPJ: ', res.json());
          const dados = {
            situacao: res.json().situacao,
            cnpj: res.json().cnpj,
            nome: res.json().nome
          }
          resolve(dados);
        },
          (err) => {
            resolve(err.json());
            // console.log('ERRO');
            // reject(JSON.parse(err))
          });
    }
    )
  }

  carregamentoNrSubnr(){
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');

    let options = new RequestOptions({
      headers: headers
    });
    // this.load();
    return new Promise((resolve, reject) => {
      this.http.post(this.api + 'Mobile/NRnova/', options)
        .subscribe(res => {
          console.log('DADO: ', res);
          var recebeComoString = JSON.stringify(res);
          let recebeComoJson = JSON.parse(recebeComoString)._body;
          // this.dismiss();
          resolve(JSON.parse(recebeComoJson));
        },
        (err) => {
          // this.dismiss();
          console.log('ERRADO: ', JSON.parse(err));
          reject(JSON.parse(err))
        });
    });
  }
}
