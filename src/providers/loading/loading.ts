import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';


@Injectable()
export class LoadingProvider {
  public loader: any;
  constructor(
    public loadingCtrl: LoadingController) {
  }

  /**
   * MÉTODO PARA INICIAR O LOADING
   */
  load() {
    this.loader = this.loadingCtrl.create({
      content: ''
    });
    this.loader.present();
  }

  loadBanco(){
    this.loader = this.loadingCtrl.create({
      content: 'Aguarde enquanto sincronizamos alguns dados. Isso pode demorar um pouco.'
    });
    this.loader.present();
  }

  /**
   * MÉTODO PARA PARAR O CARREGAMENTO DO LOADING
   */
  dismiss() {
    if (this.loader) { 
      this.loader.dismiss(); 
      this.loader = null; 
    }
  }
}
