
import { Injectable } from '@angular/core';

@Injectable()
export class MenuProvider {

  constructor() {   }

  getSideMenus() {
    return [
      // {
      //   title: 'Home', 
      //   icon: 'home', 
      //   count: 0, 
      //   component: 'HomePage'
      // },
      // {
      //   title: 'Diagnóstico',
      //   icon: 'list-box',
      //   count: 0,
      //   subPages: [{
      //     title: 'Adicionar',
      //     icon: 'add-circle',
      //     count: 0,
      //     component: 'AddDiagnosticoPage',
      //   }, 
      //   {
      //     title: 'Listar',
      //     icon: 'list',
      //     count: 0,
      //     component: 'AuditoriaTabPage',
      //   }]
      // },
      {
        title: 'Início',
        icon: 'home',
        count: 0,
        component: 'HomePage'
      },
      {
        title: 'Diagnóstico',
        icon: 'add-circle',
        count: 0,
        component: 'AuditoriaTabPage'
      },
      {
        title: 'NR',
        icon: 'bookmarks',
        count: 0,
        component: 'NormasPage'
      }
    ];
  }
}
