import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { MenuProvider } from '../providers/menu/menu';
import { PopoverComponent } from '../components/popover/popover';
import { LoadingProvider } from '../providers/loading/loading';
import { IonicStorageModule } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { StorageProvider } from '../providers/storage/storage';
import { ServiceProvider } from '../providers/service/service';
import { PopoverNovaComponent } from '../components/popover-nova/popover-nova';

@NgModule({
  declarations: [
    MyApp,
    PopoverComponent,
    PopoverNovaComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, { tabsPlacement: 'top' }),
    HttpModule,
    IonicStorageModule.forRoot({
      name: 'smsfix',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PopoverComponent,
    PopoverNovaComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {
      provide: ErrorHandler, 
      useClass: IonicErrorHandler
    },
    MenuProvider,
    DatePipe,
    LoadingProvider,
    StorageProvider,
    ServiceProvider
  ]
})
export class AppModule {}
