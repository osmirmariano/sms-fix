/**
 * COMPONENTS
 */
import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
/**
 * PROVIDERS
 */
import { MenuProvider } from './../providers/menu/menu';
import { StorageProvider } from '../providers/storage/storage';


@Component({
  templateUrl: 'app.html',
  queries: {
    nav: new ViewChild('content')
  }
})
export class MyApp {
  rootPage:any;
  private emailUser: any;
  // private tamanho: any;

  @ViewChild(Nav) nav: Nav;
  // pages: Array<{ title: string, icon: string, count: number, component: any }>;
  pages: any;
  selectedMenu:  any;

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public menuProvider: MenuProvider,
    public menuCtrl: MenuController,
    private storageProvider: StorageProvider) {
    
      this.initializeApp();

      /*
      this.pages = [
        {
          title: 'Início',
          icon: 'home',
          count: 0,
          component: 'HomePage'
        },
        {
          title: 'Diagnóstico',
          icon: 'add-circle',
          count: 0,
          component: 'AuditoriaTabPage'
        },
        {
          title: 'NR',
          icon: 'bookmarks',
          count: 0,
          component: 'NormasPage'
        }
      ];
      */

      if(JSON.parse(window.localStorage.getItem('statusLogin')) == true){
        this.rootPage = 'HomePage';
      }
      else{
        this.rootPage = 'LoginPage';
      }
      if(JSON.parse(window.localStorage.getItem('nome')) != null){
        this.emailUser = JSON.parse(window.localStorage.getItem('nome')).nome;
      }

      
      this.storageProvider.tamanho()
      .then(tamanho => {
        console.log('TAMANHO: ', tamanho);
        if(tamanho != 376){
          this.storageProvider.carregamentoNR();
          this.storageProvider.insertNRSubnr();
          // this.storageProvider.carregar();
          // this.storageProvider.carregamentoNR();
          // this.storageProvider.carregamentoSubnr();
          // this.storageProvider.getTodasNr()
          // this.storageProvider.storangeOpcao();
        }
      });
      this.storageProvider.strongeAuditoria();
  }

  /**
   * MÉTODO PARA SAIR DO APLICATIVO
   */
  initializeApp(){
    this.platform.ready().then(() => {
      this.getSideMenuData();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  /**
   * MÉTODO PARA INCIAR O MENU DO APLICATIVO
   */
  getSideMenuData() {
    this.pages = this.menuProvider.getSideMenus();
  }

  /**
   * MÉTODO PARA SAIR DO APLICATIVO
   */
  sairApp(){
    window.localStorage.removeItem('nome');
    window.localStorage.removeItem('statusLogin');
    this.nav.setRoot('LoginPage');
  }


  /**
   * MÉTODO PARA ABRIR A PÁGINAS DE ACORDO COM O MENU
   * @param page 
   * @param index 
   */
  openPage(page, index) {
    if (page.component) {
      this.nav.setRoot(page.component);
      this.menuCtrl.close();
    } 
    else {
      if (this.selectedMenu) {
        this.selectedMenu = 0;
      } 
      else {
        this.selectedMenu = index;
      }
    }
  }
}

