import { FormControl } from '@angular/forms';

//Classe para validação de e-mail de acordo com Regex
export class EmailValidator {
  static isValid(control: FormControl) {
    var regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(control.value);
    if (regexEmail) {
      return null;
    }
    return { "invalidEmail": true };
  }
}

