export class Diagnostico{
    nomeAuditor: string;
    cnpj: string;
    empresa: string;
    setor: string;
    numFuncionario: number;
    registroMTE: string;
    cnae: string;
    tipo: number;
    // area: number;
    dataCriacao: string;
    dataPrevista: string;
    usuario: string;
    grauRisco: number;
    descricao: string;
    // selecionado: any[];
}