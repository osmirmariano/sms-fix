import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, ModalController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';

@IonicPage()
@Component({
  selector: 'page-coleta-dados-auditorias',
  templateUrl: 'coleta-dados-auditorias.html',
})
export class ColetaDadosAuditoriasPage {
  public requisitos: any;
  public listaCliente: any;
  public listaNormas: any;
  public perguntas: any;
  public model: any;
  public vetorResposta: any = Array();
  public check: boolean = false;
  public descricao: any = Array();//Para o vetor das descrições dos campos que não atende e atende parcial
  public controleVisual: any = 0; //Variável para realizar o controle de mostrar e não mostrar o textarea
  public controleIncremento: any;
  public controleItem: any;
  public coletaDados: any;
  public respostaNParcial: any = Array();
  public selectedDiag: any;
  public vetorColeta: any = Array();
  public controleRadio: any = Array();
  public controleRadio2: any = Array();
  public controle: number = 0;
  public controle2: number = 0;
  public cssMostra: any;
  public controleNr: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public serviceProvider: ServiceProvider,
    public alertCtrl: AlertController,
    private modalCtrl: ModalController) {
        this.coletaDados = navParams.get('audSelecionado');
        console.log('TUDO AUDISELECIONADO: ', navParams.get('audSelecionado'));

      // console.log('REQUISITOS: ', this.requisitos);
      this.perguntasAuditoria();
      this.model = new perguntasAvalia();
      console.log('DADDADDDD: ', this.vetorColeta);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    this.novoMetodoColeta();
    this.carregaAuditoria();
  }
  
  /*
    MÉTODO PARA LISTAR NORMAS
  */
  // listarNormas(){
  //   this.serviceProvider.carregamentoPorDemandaNR()
  //   .then(data => {
  //     this.listaNormas = data;
  //     console.log('SÓ NR: ', this.listaNormas);
  //   })
  //   .catch(erro => {
  //     console.log(erro);
  //   })
  // }


  carregaAuditoria() {
    this.serviceProvider.auditoria()
      .then(data => {
        if (data) {
          let dados = JSON.stringify(data);
          this.listaCliente = JSON.parse(dados).clientes;
        }
      })
      .catch(erro => {
        console.log('DEU ERRO');
      })
  }

  perguntasAuditoria(){
    this.serviceProvider.perguntas()
    .then(data => {
      this.perguntas = data;
      console.log('PERGUNTAS: ', this.perguntas);
    })
    .catch(erro => {
      console.log('ERRO');
    })
  }

  alertaModal(valor: any) {
    var descricao;
    if(valor == 1){
      descricao = "Atende parcial"
    }
    else if(valor == 0){
      descricao = "Não atende"      
    }
    const prompt = this.alertCtrl.create({
      title: descricao,
      message: "Forneça as informações referente ao item",
      inputs: [
        {
          name: 'descricao',
          placeholder: 'Descrição'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: data => {
            this.respostaNParcial.push(data);
            console.log('Saved clicked', data);
          }
        }
      ]
    });
    prompt.present();
  }

  checarPerguntas(value: any, porcentagem: string, item: any, index: number, infracao: any, tamRequisitos: any, indexNr: any){
    let x;
    var descricao, recebe;
    
    let a;    
    console.log(document.getElementById('marcadoResposta' + index))
    if(index == 0){
      this.controle = 0;
      this.controleRadio2.push(index+1);
    }
    else{
      this.controleRadio2.push(index+1);
    }
    this.controle++;
    if(this.controle == tamRequisitos){
      this.controleRadio[this.controle2] = this.controleRadio2;
      this.controleNr = this.controle;
      this.controle2++;
      this.controleRadio2 = [];
    }

    console.log('CONTROLE: ', this.controleRadio);
    console.log('TAM: ', tamRequisitos);
    console.log('CONTADOR: ', this.controle);

    for(x = 0; x < this.vetorResposta.length; x++){
      if(this.vetorResposta[x].requisito ==  item.requisito){
        this.vetorResposta.splice(x, this.vetorResposta.length);
      }
    }

    if(porcentagem == "50" || porcentagem == "0"){
      this.controleVisual = 1;
      this.controleIncremento = index;
      this.controleItem = item.idquestoesRel;

      if(value == 1){
        descricao = "Atende parcial"
      }
      else if(value == 0){
        descricao = "Não atende"      
      }
      const prompt = this.alertCtrl.create({
        title: descricao,
        message: "Forneça as informações referente ao item",
        inputs: [
          {
            name: 'descricao',
            placeholder: 'Descrição'
          },
        ],
        buttons: [
          {
            text: 'Cancelar',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Ok',
            handler: data => {
              this.vetorResposta.push({
                resposta: value,
                requisito:  item.requisito,
                idquestoesRel: item.idquestoesRel,
                porcentagem: porcentagem,
                descricao: data.descricao,
                infracao: infracao
              })
            }
          }
        ]
      });
      prompt.present();
    }
    else{
      this.controleVisual = 0;
      this.vetorResposta.push({
        resposta: value,
        requisito:  item.requisito,
        idquestoesRel: item.idquestoesRel,
        porcentagem: porcentagem,
        descricao: '',
        infracao: infracao
      })
    }
    console.log('DADOS: ', this.vetorResposta);
  }

  salvar(){    
    this.serviceProvider.enviaColeta(this.vetorResposta)
    .then(data => {
      let modal = this.modalCtrl.create('ModalColetaContPage', {resposta: data});
      modal.present();
      console.log('DADOS ENVIADO COM SUCESSO');
    })
    .catch(erro => {
      console.log('DADOS NÂO ENVIADO COM SUCESSO');
    })
  }

  expandeRequisitos(subNormas, posicao){
    if (this.selectedDiag) {
      this.selectedDiag = 0;
    }
    else {
      this.selectedDiag = posicao;
    }
    this.vetorColeta.map((listItem) => {
      if (subNormas == listItem) {
        listItem.expanded = !listItem.expanded;
      }
      else {    
        listItem.expanded = false;
      }
      return listItem;
    });
  }

  novoMetodoColeta(){
    var cont = 0;
    this.serviceProvider.carregamentoPorDemandaNR()
    .then(data => {
      this.listaNormas = data;
      this.listaNormas.forEach(normas => {
        this.coletaDados.forEach(sub => {
          if(normas.idNR == sub.nr_NumNR){
            console.log('COLETADADOS: ', sub);
            this.vetorColeta.push({
              numNr: normas.NumNR,
              subNR: sub.subNR,
              numSub: sub.numero,
              requisito: sub.requisito
            })
          }
        });
      });
      
    })
    .catch(erro => {
      console.log(erro);
    })
  }
}

export class perguntasAvalia{
  pergunta:any;
}