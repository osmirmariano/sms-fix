import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ColetaDadosAuditoriasPage } from './coleta-dados-auditorias';
import { ServiceProvider } from '../../providers/service/service';
import { ExpandeColetaComponent } from '../../components/expande-coleta/expande-coleta';

@NgModule({
  declarations: [
    ColetaDadosAuditoriasPage,
    ExpandeColetaComponent
  ],
  imports: [
    IonicPageModule.forChild(ColetaDadosAuditoriasPage),
  ],
  exports: [
    ColetaDadosAuditoriasPage
  ],
  providers:[
    ServiceProvider
  ]
})
export class ColetaDadosAuditoriasPageModule {}
