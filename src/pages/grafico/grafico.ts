import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';

declare var google;

@IonicPage()
@Component({
  selector: 'page-grafico',
  templateUrl: 'grafico.html',
})

export class GraficoPage {
  public grafico: any;
  public graficoResultado: any;
  public subTitulo: any = Array();
  public listaNrs: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public serviceProvider: ServiceProvider) {
                
  }

  /**
   * MÉTODO DO ANGULAR PARA CARREGAR ANTES DE A PAGE SER CARREGADA COMPLETAMENTE
   */
  ngOnInit(){
    this.graficoResultado = this.navParams.get('objetoGrafico');
  }

  /**
   * MÉTODO É ACIONADO ANTES DO INÍCIO DA PAGE
   */  
  ionViewDidEnter(){
    this.graficoSubtitulo();
  }

  /**
   * MÉTODO PARA GERAR OS DADOS DO GRÁFICO DE ACORDO COM A AUDITORIA
   */
  graficoSubtitulo(){
    let x, y, z, contador, calculo, nr;
    nr = JSON.parse(window.localStorage.getItem('nr'));
    var vetorGrafico = [];
    for(z = 0; z < nr.length; z++){
      for(x = 0; x < this.graficoResultado.length; x++){
        if(nr[z].idNR == this.graficoResultado[x].nr_NumNR){
          console.log('REQ: ', this.graficoResultado[x]);
          contador = 0;
          for(y = 0; y < this.graficoResultado[x].requisito.length; y++){
            if(this.graficoResultado[x].requisito[y].porcentagem == 100){
              contador++;
            }

            if(y == this.graficoResultado[x].requisito.length-1 && contador != 0){
              calculo = contador*100/this.graficoResultado[x].requisito.length;
              vetorGrafico.push({
                descricao: 'NR '+nr[z].NumNR + ' - ' + this.graficoResultado[x].numero,
                totalAtende: contador,
                totalTodos: this.graficoResultado[x].requisito.length,
                calculo: calculo
              })
              this.mostrarGrafico(vetorGrafico, this.graficoResultado);
            }
          }
        }
      }
    }
  }
  
  /**
   * MÉTODO PARA MOSTRAR GRÁFICO
   * @param vetorGrafico 
   * @param grafico 
   */
  mostrarGrafico(vetorGrafico, grafico){
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Descrição');
    data.addColumn('number', '% Requisitos \natendem');
    data.addColumn('number', '% Requisitos \nnão atendem');
    
    for(var i = 0; i < vetorGrafico.length; i++){   
      data.addRow([vetorGrafico[i].descricao, vetorGrafico[i].calculo, 100-vetorGrafico[i].calculo]);
    }

    var options = {
      isStacked: true,
      // width: 100,
      height: 500,
      // legend: { 
      //   position: 'none'
      // },
      legend: { position: 'top', alignment: 'start' },
      chart: {
        title: 'Gráfico auditoria/inspeção',
      },
      hAxis: {
        title: '',
      },
      vAxis: {
        title: '% de requisitos que atendem e não atendem.'
      },
      bar: { 
        groupWidth: "20%",
        // groupHiegth: "200px",
      },
      colors: ['#0ba034'],
    };

    var chart = new google.charts.Bar(document.getElementById('chart_div'));
    chart.draw(data, google.charts.Bar.convertOptions(options));

    let img = document.getElementById('chart_div');
  }

  gerarGraficoPDF(){

  }

  /**
   * MÉTODO PARA FECHAR O MODAL DA PAGE GRÁFICO
   */
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
