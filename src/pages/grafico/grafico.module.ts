import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GraficoPage } from './grafico';
// import { ChartsModule } from 'ng2-charts';
import { ServiceProvider } from '../../providers/service/service';
// import { Chart } from 'chart.js';

@NgModule({
  declarations: [
    GraficoPage,
    
  ],
  imports: [
    IonicPageModule.forChild(GraficoPage),
    // Chart
    // ChartsModule,
  ],
  exports: [
    GraficoPage
  ],
  providers: [
    ServiceProvider
  ]
})
export class GraficoPageModule {}
