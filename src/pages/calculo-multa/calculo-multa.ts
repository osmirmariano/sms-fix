import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Multas } from '../../models/multas';
import { FormBuilder, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-calculo-multa',
  templateUrl: 'calculo-multa.html',
})

export class CalculoMultaPage {
  model: Multas;
  public multaForm;
  multaMinima: any;
  multaMaxima: any;


  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private formularioBuilder: FormBuilder,
    private alertCtrl: AlertController) {

      this.multaForm = formularioBuilder.group({
        tipo: ['', Validators.compose([Validators.required])],
        infracao: ['', Validators.compose([Validators.required])],
        numFuncionario: ['', Validators.compose([Validators.required])]
      });

      this.model = new Multas();
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalculoMultaPage');
  }

  multas(){
    if(!this.multaForm.valid){
      let alert = this.alertCtrl.create({
        title: 'Campos obrigatórios',
        subTitle: 'É necessário preencher todos os campos.',
        buttons: ['OK']
      });
      alert.present();
    }
    else{
      this.multaMinima = 10;
      this.multaMaxima = 40;
      console.log('MULTAS: ', this.model);
    }
  }
}
