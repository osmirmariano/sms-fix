import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalculoMultaPage } from './calculo-multa';

@NgModule({
  declarations: [
    CalculoMultaPage,
  ],
  imports: [
    IonicPageModule.forChild(CalculoMultaPage),
  ],
  exports: [
    CalculoMultaPage
  ]
})
export class CalculoMultaPageModule {}
