import { ExpandeItemComponent } from './../../components/expande-item/expande-item';
import { ServiceProvider } from './../../providers/service/service';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NormasPage } from './normas';
/*PLUGIN PARA TRATAMENTO DE ARQUIVOS, PDFS E VISUALIZAR*/
import { DocumentViewer } from '@ionic-native/document-viewer';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { FileOpener } from '@ionic-native/file-opener';
import { LoadingProvider } from '../../providers/loading/loading';

@NgModule({
  declarations: [
    NormasPage,
    ExpandeItemComponent
  ],
  imports: [
    IonicPageModule.forChild(NormasPage),
    PdfViewerModule
  ],
  exports: [
    NormasPage
  ],
  providers: [
    ServiceProvider,
    DocumentViewer,
    File,
    FileTransfer,
    FileOpener,
    LoadingProvider
  ]
})
export class NormasPageModule {}
