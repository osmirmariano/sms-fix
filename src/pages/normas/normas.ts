import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, ToastController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { FileOpener } from '@ionic-native/file-opener';
import { LoadingProvider } from '../../providers/loading/loading';

@IonicPage()
@Component({
  selector: 'page-normas',
  templateUrl: 'normas.html',
})
export class NormasPage {
  public nrsLista: any;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public serviceProvider: ServiceProvider,
    private platform: Platform,
    private document: DocumentViewer,
    private file: File,
    private transfer: FileTransfer,
    private fileOpener: FileOpener,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public loadingProvider: LoadingProvider) {      
  }

  ionViewDidLoad() {
    this.loadingProvider.load();
    this.serviceProvider.carregamentoPorDemandaNR()
    .then(nrs => {
      this.loadingProvider.dismiss();
      this.nrsLista = nrs;
    })
  }

 
  abrirPDFNrs(index){
    let me = this;
    this.platform.ready()
    .then(() => {
      let loading = me.loadingCtrl.create({
        content: 'Preparando o arquivo...'
      })
      loading.present();

      this.file.createDir(this.file.externalDataDirectory, "smsfix", true)
      .then(link => {
        const transfer = this.transfer.create();
        let url = 'http://smsfix.com.br/sms-fix/assets/uploads/NR/NR'+index+'.pdf';
        transfer.download(url, this.file.externalDataDirectory + "smsfix/nrs/NR"+index+".pdf")
        .then((down) => {
          console.log('DHAHDHADHHAD: ', down.toURL());
          loading.dismiss();
          this.fileOpener.open(down.toURL(), 'application/pdf')
          .then((certo) => {
            console.log('SUCESSO: ', certo);
          }, erro => {
            console.log("status : "+erro.status);
            console.log("error : "+erro.message);
          });
        },(error) => {
          loading.dismiss();
          let toast = this.toastCtrl.create({
            message: 'Não foi possível abrir o arquivo.',
            cssClass: 'normas',
            duration: 3000,
            position: 'bottom'
          }).present();
          console.log('AQUI 1: ', error);
        });
      }, (error) => {
        loading.dismiss();
        let toast = this.toastCtrl.create({
          message: 'Não foi possível abrir o arquivo.',
          cssClass: 'normas',
          duration: 3000,
          position: 'bottom'
        }).present();
        console.log('AQUI 2:', error);
      });
    });
  }
}
