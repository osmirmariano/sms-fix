import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, MenuController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { EmailValidator } from '../../validators/email';
import { ServiceProvider } from '../../providers/service/service';
import { Users } from '../../models/users';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  model: Users;
  public loginForm;
  show = false;
  type = "password";
  ver: boolean = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private formularioBuilder: FormBuilder,
    public serviceProvider: ServiceProvider,
    private toast: ToastController,
    public menuCtrl: MenuController) {

      this.loginForm = formularioBuilder.group({
        email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
        senha: ['', Validators.compose([Validators.minLength(8), Validators.required])]
      });
  
      this.model = new Users();
  }

  ionViewDidLoad() { }
  
  ionViewDidEnter(){
    this.menuCtrl.enable(false);
    this.menuCtrl.swipeEnable(false);
  }

  login(){
    this.serviceProvider.login(this.model.email, this.model.senha)
    .then(data => {
      if(data){
        var statusLogin = JSON.stringify(data);
        window.localStorage.setItem('statusLogin', JSON.stringify(JSON.parse(statusLogin).status));
        if(JSON.parse(statusLogin).status == true){
          this.navCtrl.setRoot('HomePage');
        }
        else{
          this.toast.create({
            message: 'Usuário e/ou senha inválida.',
            position: 'bottom',
            cssClass: 'erro-login',
            duration: 7000
          }).present();
        }
      }
    })
    .catch(erro => {
      this.toast.create({
        message: 'Não foi possível realizar o login.',
        position: 'bottom',
        cssClass: 'erro-login',
        duration: 3000
      }).present();
    })
  }


  //Para mostrar a senha na tela de login
  visualizarSenha(){
    this.show = !this.show;
    if (this.show){
      this.type = "text";
      this.ver = true;
    }
    else {
      this.type = "password";
      this.ver = false;
    }
  }

  chamarCriarConta(){
    this.navCtrl.push('CriarContaPage');
  }
}


