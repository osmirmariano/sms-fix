import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VisualizarPage } from './visualizar';

@NgModule({
  declarations: [
    VisualizarPage,
  ],
  imports: [
    IonicPageModule.forChild(VisualizarPage),
  ],
  exports: [
    VisualizarPage
  ]
})
export class VisualizarPageModule {}
