import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-visualizar',
  templateUrl: 'visualizar.html',
})
export class VisualizarPage {
  public viewClient: any;
  public viewAudit: any;
  public tipo: any;
  public area: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
     public viewCtrl: ViewController) {
      this.viewClient = navParams.get('selecionado');
      this.viewAudit = navParams.get('auditoria');
      console.log('LISTA DE CLIENTES: ', this.viewClient);
      console.log('LISTA DE AUDITORIAS: ', this.viewAudit);
      //Condição para ao invés do número colocar texto
      this.viewAudit.tipo == 1 ? this.area = 'Segurança do trabalho': this.area = 'Medicina do trabalho';
      this.viewAudit.area == 1 ? this.tipo = 'Auditoria': this.tipo = 'Inspeção';
      console.log('TIPO: ', this.area);
      console.log('TIPO: ', this.tipo);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  
}
