import { BrMaskerModule } from 'brmasker-ionic-3';
import { ServiceProvider } from './../../providers/service/service';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddDiagnosticoPage } from './add-diagnostico';
import { DatePicker } from '@ionic-native/date-picker';
import { LoadingProvider } from '../../providers/loading/loading';

@NgModule({
  declarations: [
    AddDiagnosticoPage,
  ],
  imports: [
    IonicPageModule.forChild(AddDiagnosticoPage),
    BrMaskerModule
  ],
  exports: [
    AddDiagnosticoPage
  ],
  providers: [
    ServiceProvider,
    DatePicker, 
    LoadingProvider
  ]
})
export class AddDiagnosticoPageModule {}
