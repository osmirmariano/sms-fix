import { ServiceProvider } from './../../providers/service/service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Keyboard, AlertController, ViewController } from 'ionic-angular';
import { Diagnostico } from '../../models/diagnostico';
import { FormBuilder, Validators } from '@angular/forms';
import { DatePicker } from '@ionic-native/date-picker';
import { CnpjValidator } from '../../validators/cnpj';
import { LoadingProvider } from '../../providers/loading/loading';

@IonicPage()
@Component({
  selector: 'page-add-diagnostico',
  templateUrl: 'add-diagnostico.html',
})

export class AddDiagnosticoPage {
  model: Diagnostico;
  public diagnositcoForm;
  recebe: boolean;
  nome: any;
  nomeEmpresa: any;
  usuarios: any = Array();
  maxDate: any;
  minDate: any;
  
  // nomeAuditor: string;
  // cnpj: string;
  // empresa: string;
  // setor: string;
  // numFuncionario: number;
  // registroMTE: string;
  // cnae: string;
  // tipo: string;
  // data: string;
  // usuario: string;
  // grauRisco: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private formularioBuilder: FormBuilder,
    public datePicker : DatePicker,
    public keyboard : Keyboard,
    public serviceProvider: ServiceProvider,
    private alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public loadingProvider: LoadingProvider) {
      this.diagnositcoForm = formularioBuilder.group({
        descricao: ['', Validators.compose([Validators.maxLength(50)])],
        nomeAuditor: ['', Validators.compose([Validators.required])],
        cnpj: ['', Validators.compose([Validators.required, CnpjValidator.isValid])],
        empresa: ['', Validators.compose([Validators.required])],
        setor: ['', Validators.compose([Validators.required])],
        numFuncionario: ['', Validators.compose([Validators.required])],
        cnae: ['', Validators.compose([Validators.required])],
        registroMTE: ['', Validators.compose([Validators.required])],
        grauRisco: ['', Validators.compose([Validators.required])],
        dataCriacao: ['', Validators.compose([Validators.required])],
        dataPrevista: ['', Validators.compose([Validators.required])],
        area: ['', Validators.compose([Validators.required])]
      });

      this.model = new Diagnostico();
      // this.model.dataPrevista = '10/02/2018';
      this.dataHoje();
  }

  /**
   * MÉTODO PARA OBTER A DATA ATUAL PARA DATA ATUAL
   */
  dataHoje() {
    var data = new Date();
    var dia = data.getDate();
    var mes = data.getMonth() + 1;
    var ano = data.getFullYear();
    var resultado = dia+"/"+mes+"/"+ano;
    this.model.dataCriacao = resultado;
    this.model.dataPrevista = resultado;
    this.minDate = this.model.dataCriacao;
  }

  /**
   * MÉTODO PARA ESCOLHER UMA DATA NO CALENDÁRIO
   */
  openDatepicker(){
    this.keyboard.close();
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
    }).then(
      date => {
        this.model.dataCriacao = date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()
      },
      err => console.log('Erro ao processar a data: ', err)
    );
  }

  /**
   * MÉTODO PARA ESCOLHER UMA DATA NO CALENDÁRIO PARA DATA PREVISTA
   */
  openDatepickerPrevista(){
    this.keyboard.close();
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
    }).then(
      date => {
        this.model.dataPrevista = date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()},
      err => console.log('Erro ao processar a data: ', err)
    );
  }

  /**
   * MÉTODO PARA OBTER OS DADOS DA PAGE ANTERIOR E CONTINUAR COM O CADASTRO
   */
  continuar(){    
    let resc: any, dadosCnpjEmpresa: any;
    this.loadingProvider.load();
    if(this.diagnositcoForm.valid){
      console.log('DADADOS DDDAD D DAD');
      this.serviceProvider.cnpj(this.model.cnpj)
      .then(resultado => {
        if(resultado){
          resc = resultado;   
          dadosCnpjEmpresa = {
            nome: resc.nome,
            cnpj: resc.cnpj
          }

          if(dadosCnpjEmpresa.none == undefined){
            dadosCnpjEmpresa = {
              nome: this.model.empresa,
              cnpj: this.model.cnpj
            }
          }
          console.log('DADOS CNPJ: ', dadosCnpjEmpresa);
          console.log('this.model: ', this.model);

          if(resc.situacao == "ATIVA"){
            this.loadingProvider.dismiss();
            this.navCtrl.setRoot('AddDiagnosticoContPage', { dados: this.model, dadosEmpresa: dadosCnpjEmpresa }); 
          }
          else{
            if(resc.situacao == undefined){
              this.loadingProvider.dismiss();
              this.navCtrl.setRoot('AddDiagnosticoContPage', { dados: this.model, dadosEmpresa: dadosCnpjEmpresa }); 
            }
            else{
              let alert = this.alertCtrl.create({
                title: 'CNPJ inválido',
                subTitle: 'CNPJ não está ativo.',
                buttons: ['OK']
              });
              alert.present();
            }
          }
        }
      })
      .catch(erro => {
        console.log('Erro no CNPJ');
      })
    }
    else{
      this.loadingProvider.dismiss();
      let alert = this.alertCtrl.create({
        title: 'Campos obrigatórios',
        subTitle: 'Por favor, verificar se todos os campos estão corretos.',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  /**
   * ESSE MÉTODO FAZ A VALIDAÇÃO DO CNPJ JUNTO A RECEITA FEDERAL
   */
  validarCNPJ(){
    this.serviceProvider.cnpj(this.model.cnpj.toString().replace('.', '').replace('.', '').replace('/', '').replace('-', ''))
    .then(cnpj => {
      console.log('DADOS: ', cnpj);
      let dadosCNPJ = JSON.stringify(cnpj);
      this.nomeEmpresa = JSON.parse(dadosCNPJ).fantasia;
    })
  }

  /**
   * PARA VERIFICAR A AUDITORIA SE O CNPJ PERTENCE ALGUMA EMPRESA, ESSE MÉTODO TÁ DEPRECISADO, REMOVER DEPOIS
   * @param cnpj 
   */
  cliente(cnpj){
    let contador = 0;
    this.serviceProvider.auditoria()
      .then(resultado => {
        if(resultado){
          let dados = JSON.stringify(resultado);
          JSON.parse(dados).clientes.forEach(element => {
            if(element.cnpj == cnpj)
              this.nome = element.nomeClientes;
            else
              contador++;
          });
          if(contador == JSON.parse(dados).clientes.length){
            this.nome = 'naoexiste';
          }
        }
      })
      .catch(erro => {
        console.log('ERRO: ', erro);
      })
  }
  
  /**
   * MÉTODO PARA MOSTRAR OS CLIENTES
   */
  mostrarUsuario(){
    this.serviceProvider.listarUsuarios()
    .then(resultado => {
      console.log('RESULTADO: ', resultado);
      this.usuarios = resultado;
    })
    .catch(erro => {
      console.log('ERRO: ', erro);
    })
  }

  /**
   * MÉTODO PARA VALIDAR CNPJ DA EMPRESA, ACHO QUE TÁ DEPRECIADO
   * @param e 
   * @param cnpj 
   */
  verificarCNPJ(e, cnpj){
    console.log('E: ', e);
    console.log('CNPJ: ', cnpj);
    this.serviceProvider.cnpj(cnpj)
    .then(resultado => {
      console.log('RESULTADO: ', resultado);
    })
    .catch(erro => {
      console.log('ERRO: ', erro);
    })
  }

  /**
   * MÉTODO PARA FECHAR O MODAL DA PAGE
   */
  dismiss(){
    this.viewCtrl.dismiss();
  }
}
