import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-auditoria-tab',
  templateUrl: 'auditoria-tab.html'
})

export class AuditoriaTabPage {
  novaRoot = 'NovaPage'
  andamentoRoot = 'AndamentoPage'
  finalizadaRoot = 'FinalizadaPage'

  constructor(public navCtrl: NavController) {

  }

}
