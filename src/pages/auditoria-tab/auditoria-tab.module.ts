import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AuditoriaTabPage } from './auditoria-tab';

@NgModule({
  declarations: [
    AuditoriaTabPage
  ],
  imports: [
    IonicPageModule.forChild(AuditoriaTabPage),
  ],
  exports: [
    AuditoriaTabPage,
  ]
})
export class AuditoriaTabPageModule {}
