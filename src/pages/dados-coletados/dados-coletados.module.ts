import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DadosColetadosPage } from './dados-coletados';

@NgModule({
  declarations: [
    DadosColetadosPage,
  ],
  imports: [
    IonicPageModule.forChild(DadosColetadosPage),
  ],
  exports: [
    DadosColetadosPage
  ]
})
export class DadosColetadosPageModule {}
