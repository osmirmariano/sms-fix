import { ServiceProvider } from './../../providers/service/service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, ModalController, AlertController, ToastController, ActionSheetController } from 'ionic-angular';
// import { PopoverComponent } from '../../components/popover/popover';
import { PopoverNovaComponent } from '../../components/popover-nova/popover-nova';


@IonicPage()
@Component({
    selector: 'page-nova',
    templateUrl: 'nova.html',
})
export class NovaPage {
    selectedDiag: any;
    listaCliente: any = Array();
    public idAuditoria: any;
    public graficoResultados: any;
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public popoverCtrl: PopoverController,
        private modalCtrl: ModalController,
        public serviceProvider: ServiceProvider,
        private alertCtrl: AlertController,
        private toastCtrl: ToastController,
        public actionSheetCtrl: ActionSheetController) {
    }

    ionViewDidLoad() {
        this.carregaAuditoria();
    }

    ionViewDidLeave() {

    }

    editar() {
        this.navCtrl.setRoot('EditarPage');
    }

    grafico() {
        this.navCtrl.push('GraficoPage');
    }

    planoAcao() {
        this.navCtrl.push('PlanoAcaoPage');
    }

    dadosColetados() {
        this.navCtrl.push('DadosColetadosPage');
    }

    carregaAuditoria() {
        this.serviceProvider.auditoria()
            .then(data => {
                if (data) {
                    let dados = JSON.stringify(data);
                    this.listaCliente = JSON.parse(dados).clientes;
                }
            })
            .catch(erro => {
                let toast = this.toastCtrl.create({
                    message: 'Não foi possível carregar a lista '+ erro,
                    duration: 3000,
                    cssClass: 'erro',
                    position: 'bottom'
                  })
                  toast.present();
            })
    }

    expandeAuditoria(item, index) {
        if (this.selectedDiag) {
            this.selectedDiag = 0;
        }
        else {
            this.selectedDiag = index;
        }

        this.listaCliente.map((listItem) => {
            if (item == listItem) {
                listItem.expanded = !listItem.expanded;
            }
            else {
                listItem.expanded = false;
            }
            return listItem;
        });
    }

    carregarGrafico() {
        this.serviceProvider.paraGrafico(this.idAuditoria)
            .then(resultado => {
                this.graficoResultados = resultado;
                window.localStorage.setItem('grafico', JSON.stringify(resultado));
            })
            .catch(erro => {
                let toast = this.toastCtrl.create({
                    message: 'Não foi possível carregar os dados ' + erro,
                    duration: 3000,
                    cssClass: 'erro',
                    position: 'bottom'
                })
                toast.present();
            })
    }

    presentPopover(myEvent, dados, x, auditoria) {
        this.idAuditoria = auditoria.idAuditoria;
        
        if (auditoria.reqAuditoria.length != 0){
            this.carregarGrafico();
        }

        let popover = this.popoverCtrl.create(PopoverNovaComponent, { cssClass: 'popover' });
        popover.present({
            ev: myEvent
        });

        popover.onDidDismiss(popoverData => {
            if (popoverData == null) {
                console.log('NULL');
            }
            else {
                if (popoverData.item == "Editar") {
                    let modal = this.modalCtrl.create('EditarPage', { selecionado: dados });
                    modal.present();
                }
                else {
                    if (popoverData.item == "Coleta de dados") {
                        let modal = this.modalCtrl.create('ColetaDadosAuditoriasPage', { index: x,  audSelecionado: this.graficoResultados });
                        modal.present();
                    }
                    else {
                        if (popoverData.item == "Visualizar") {
                            let modal = this.modalCtrl.create('VisualizarPage', { selecionado: dados, auditoria: auditoria });
                            modal.present();
                        }
                        else {
                            if (popoverData.item == "Excluir") {
                                let actionSheet = this.actionSheetCtrl.create({
                                    title: 'Deseja realmente excluir',
                                    buttons: [
                                      {
                                        icon: 'checkmark-circle',
                                        text: 'Sim',
                                        cssClass: 'sim',
                                        handler: () => {
                                          console.log('Aqui é para excluir');
                                        }
                                      },
                                      {
                                        icon: 'close-circle',
                                        text: 'Não',
                                        cssClass: 'cancelar',
                                        handler: () => {
                                          console.log('Aqui não vai fazer nada');
                                        }
                                      }
                                    ]
                                });
                                actionSheet.present();
                            }
                        }
                    }
                }
            }
            console.log(popoverData);
        })
    }

    adicionarAuditoria(){
        let modal = this.modalCtrl.create('AddDiagnosticoPage');
        modal.present();
    }
}
