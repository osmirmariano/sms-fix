import { ServiceProvider } from './../../providers/service/service';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NovaPage } from './nova';
import { ExpandeAuditoriaComponent } from '../../components/expande-auditoria/expande-auditoria';

@NgModule({
  declarations: [
    NovaPage,
    ExpandeAuditoriaComponent
  ],
  imports: [
    IonicPageModule.forChild(NovaPage),
  ],
  providers: [
    ServiceProvider
  ],
  exports: [
    NovaPage
  ]
})
export class NovaPageModule {}
