import { ServiceProvider } from './../../providers/service/service';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinalizadaPage } from './finalizada';
import { ExpandeFinalizadaComponent } from '../../components/expande-finalizada/expande-finalizada';

@NgModule({
  declarations: [
    FinalizadaPage,
    ExpandeFinalizadaComponent
  ],
  imports: [
    IonicPageModule.forChild(FinalizadaPage),
  ],
  exports: [
    FinalizadaPage
  ],
  providers:[
    ServiceProvider
  ]
})
export class FinalizadaPageModule {}
