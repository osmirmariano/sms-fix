import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, ModalController, AlertController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { PopoverComponent } from '../../components/popover/popover';

@IonicPage()
@Component({
  selector: 'page-finalizada',
  templateUrl: 'finalizada.html',
})
export class FinalizadaPage {
  selectedDiag: any;
  listaCliente: any = Array();
  public idAuditoria: any;
  public graficoResultados: any;
  
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    private modalCtrl: ModalController,
    public serviceProvider: ServiceProvider,
    private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    this.carregaAuditoria();
  }
  
  carregaAuditoria() {
    this.serviceProvider.auditoria()
      .then(data => {
        if (data) {
          let dados = JSON.stringify(data);
          this.listaCliente = JSON.parse(dados).clientes;
          console.log('RESULTADO CLIENTES: ', this.listaCliente);
        }
      })
      .catch(erro => {
        console.log('DEU ERRO');
      })
  }

  expandeAuditoria(item, index) {
    if (this.selectedDiag) {
      this.selectedDiag = 0;
    }
    else {
      this.selectedDiag = index;
    }

    this.listaCliente.map((listItem) => {
      if (item == listItem) {
        listItem.expanded = !listItem.expanded;
      }
      else {
        listItem.expanded = false;
      }
      return listItem;
    });
  }

  carregarGrafico() {
    console.log('ID: ', this.idAuditoria);
    this.serviceProvider.paraGrafico(this.idAuditoria)
      .then(resultado => {
        this.graficoResultados = resultado;
        window.localStorage.setItem('grafico', JSON.stringify(resultado));
        console.log('RESULTADO AQUI: ', resultado);
      })
      .catch(erro => {
        console.log('DEU ERRO');
      })
  }

  presentPopover(myEvent, dados, x, auditoria) {

    console.log('AUDITORIA: ', auditoria);
    this.idAuditoria = auditoria.idAuditoria;
    if (auditoria.reqAuditoria.length != 0){
      this.carregarGrafico();
    }
    
    let popover = this.popoverCtrl.create(PopoverComponent, { cssClass: 'popover' });
    popover.present({
      ev: myEvent
    });

    popover.onDidDismiss(popoverData => {
      if (popoverData == null) {
        console.log('NULL');
      }
      else {
        if (popoverData.item == "Editar") {
          let modal = this.modalCtrl.create('EditarPage', { selecionado: dados });
          modal.present();
        }
        else {
          if (popoverData.item == "Coleta de dados") {
            let alert = this.alertCtrl.create({
              title: 'Coleta de dados não autorizada',
              subTitle: 'Não é possível realizar a coleta de dados, pois a auditoria já foi encerrada.',
              buttons: ['OK']
            });
            alert.present();

            // let modal = this.modalCtrl.create('ColetaDadosAuditoriasPage', { selecionado: auditoria, index: x });
            // modal.present();
          }
          else {
            if (popoverData.item == "Visualizar") {
              let modal = this.modalCtrl.create('VisualizarPage', { selecionado: dados, auditoria: auditoria });
              modal.present();
            }
            else {
              if (popoverData.item == "Gráfico") {
                let modal = this.modalCtrl.create('GraficoPage', { grafico: this.idAuditoria, objetoGrafico: this.graficoResultados });
                modal.present();
              }
              else {
                if (popoverData.item == "Plano de ação") {
                  let modal = this.modalCtrl.create('PlanoAcaoPage', { planoacao: this.graficoResultados, index: x });
                  modal.present();
                }
              }
            }
          }
        }
      }
      console.log(popoverData);
    })
  }
  
  adicionarAuditoria(){
    let modal = this.modalCtrl.create('AddDiagnosticoPage');
    modal.present();
  }
}
