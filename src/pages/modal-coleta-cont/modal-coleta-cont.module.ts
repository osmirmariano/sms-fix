import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalColetaContPage } from './modal-coleta-cont';
import { ServiceProvider } from '../../providers/service/service';

@NgModule({
  declarations: [
    ModalColetaContPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalColetaContPage),
  ],
  exports: [
    ModalColetaContPage
  ],
  providers: [
    ServiceProvider
  ]
})
export class ModalColetaContPageModule {}
