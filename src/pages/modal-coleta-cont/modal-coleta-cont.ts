import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, Platform } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { Parecer } from '../../models/parecer';
import { ServiceProvider } from '../../providers/service/service';

@IonicPage()
@Component({
  selector: 'page-modal-coleta-cont',
  templateUrl: 'modal-coleta-cont.html',
})
export class ModalColetaContPage {
  public parecerFrom;
  model: Parecer;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private formularioBuilder: FormBuilder,
    public viewCtrl: ViewController,
    public serviceProvider: ServiceProvider,
    private alertCtrl: AlertController,
    public platform: Platform) {

      this.parecerFrom = formularioBuilder.group({
        critico: ['', Validators.compose([Validators.maxLength(400), Validators.required])],
        parecer: ['', Validators.compose([Validators.maxLength(400), Validators.required])],
      });
      this.model = new Parecer();
      //Ação para impedir que o botão voltar seja acionado
      platform.registerBackButtonAction(() => { },1);
  }
    
  ionViewDidLeave(){ }

  enviaColetaDados(){
    if(this.parecerFrom.valid){
      this.serviceProvider.finalizarColeta(this.navParams.get('resposta'), this.model)
        .then(resultado => {
          this.navCtrl.setRoot('AuditoriaTabPage');
          console.log('Deu Certo: ', resultado);
        })
    }
    else{
      let alert = this.alertCtrl.create({
        title: 'Campos obrigatórios',
        subTitle: 'Por favor, verificar se todos os campos estão corretos.',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}

