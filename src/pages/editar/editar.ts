import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-editar',
  templateUrl: 'editar.html',
})
export class EditarPage {
  public editar: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController) {
      this.editar = navParams.get('selecionado');
      console.log('DADOS CHEGOU AQUI: ', this.editar);
  }


  dismiss() {
    this.viewCtrl.dismiss();
  }

}
