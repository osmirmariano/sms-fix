import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform, MenuController, ToastController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { FormBuilder } from '@angular/forms';
import { StorageProvider } from '../../providers/storage/storage';
import { LoadingProvider } from '../../providers/loading/loading';


@IonicPage()
@Component({
  selector: 'page-add-diagnostico-cont',
  templateUrl: 'add-diagnostico-cont.html',
})
export class AddDiagnosticoContPage {
  public listaNormas: any;
  public normas: any;
  public subNormas: any;
  public itemNormas: any;
  selectedDiag: any;
  public diagnositcoForm;
  public model;
  public verificaStatus: any = "naoVerifica";

  public checkNormas: any = false;
  public checkSub: any = false;
  public checkItens: any = false;
  public recebeCheckSub: any;
  public recebeCheckItem: any;
  public controle: any;
  public enviaItem: any[] = Array();
  public tratamento: any = Array();
  public novoTratamento: any = Array();
  public vetorFinal: any = Array();
  public arraySubNormas: any = Array();
  public verificaMarcado: any = Array();
  public listarClickNormas: any = Array();
  public contad = 0;
  public vetorVerifica: any = Array();
  public requisitos: any = Array();
  public vetorContador: any = Array();
  public vetorTratamento: any = Array();
  public arrayNRs: any = Array();
  public posicao = 0;
  public contador;
  public idmarcados: any = Array();
  // public reqmarcados: any = Array();
  public contchamada;
  public tamMarcado: any = Array();
  public receitaCnpj: any;
  public listarRequisitos: any = Array();
  public visual: any = 'auditoria';
  public incremento = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceProvider: ServiceProvider,
    private formularioBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private storageProvider: StorageProvider,
    public loadingProvider: LoadingProvider,
    public platform: Platform,
    public menuCtrl: MenuController,
    private toastCtrl: ToastController) {

    this.diagnositcoForm = formularioBuilder.group({});
    this.serviceProvider.cnpj(this.navParams.get('dados').cnpj)
      .then(certo => {
        this.receitaCnpj = certo;
      });

    //Ação para impedir que o botão voltar seja acionado
    platform.registerBackButtonAction(() => { }, 1);
  }

  dismiss() {
    this.navCtrl.setRoot('AuditoriaTabPage');
  }

  /**
   * MÉTODO PARA CARREGAR AS NORMAS ASSIM QUE A PAGE FOR CARREGADA
   */
  ionViewWillEnter() {
    // this.storageProvider.getTodasNr()
    //   .then(result => {
    //     this.listaNormas = result;
    //   })
    //   .catch(erro => {
    //     console.log('ERRO: ', erro);
    //   });
    // this.loadingProvider.load();
    this.menuCtrl.enable(true);
    this.menuCtrl.swipeEnable(true);

    this.storageProvider.getNRSubnr()
      .then(result => {
        // this.loadingProvider.dismiss();
        let re = JSON.stringify(result);
        this.listaNormas = JSON.parse(re).nr;
      })
      .catch(erro => {
        let toast = this.toastCtrl.create({
          message: 'Não foi possível carregar as normas '+ erro,
          duration: 3000,
          cssClass: 'erro',
          position: 'bottom'
        })
        toast.present();
      });
  }
  
  incrementar(subNr) {
  
  }

  /**
   * MÉTODO PARA PREENCHER OS CHECKBOX DAS SUBNORMAS, NESSE ELE VAI SELECIONAR SOMENTE QUANDO CLICAR NELE
   * @param idSub 
   * @param tipo 
   */

  
  checarSub(idSub: any) {
    console.log('ID: ', idSub);
    let sub = document.getElementsByClassName('subNr' + idSub);
    console.log('DIADIAIDID: ', document.getElementsByClassName('subnrAtivado' + idSub));
    let subAtivado = document.getElementsByClassName('subnrAtivado' + idSub);
    let subDesativado = document.getElementsByClassName('subnrDesativado' + idSub);
    
    let pegaSelectSubAtivado = document.getElementById('selectSubAtivado' + idSub);
    let pegaSelectSubDesativado = document.getElementById('selectSubDesativado' + idSub);
    let x;
    
    console.log('SUB: ', sub);
    console.log('SUBATIVADO: ', subAtivado);
    console.log('SUBDESATIVADO: ', subDesativado);
    console.log('pegaSelectSubAtivado: ', pegaSelectSubAtivado);
    console.log('pegaSelectSubDesativado: ', pegaSelectSubDesativado);

    this.contchamada = 0;
    if (pegaSelectSubAtivado.style.display == 'none') {
      console.log('TAM: ', sub.length);
      for (x = 0; x < sub.length; x++) {
        if (subAtivado != null) {
          console.log('subAtivado: ', subAtivado);
          if (sub[x].classList[1] != 'marcado' + idSub) {
            sub[x].classList.add('marcado' + idSub);
            console.log('SUBATIVADO: ', document.getElementById('subnrAtivado'+idSub));
            console.log('SUBDESATIVADO: ', document.getElementById('subnrDesativado'+idSub));
            // document.getElementById('subnrAtivado'+idSub).style.display = 'block';
            // document.getElementById('subnrDesativado'+idSub).style.display = 'none';
            
            // document.getElementById(subAtivado[x].id).style.display = 'block';
            // document.getElementById(subDesativado[x].id).style.display = 'none';
            pegaSelectSubAtivado.style.display = 'block';
            pegaSelectSubDesativado.style.display = 'none';
          }
        }
      }
    }
    else {
      for (x = 0; x < sub.length; x++) {
        console.log('STATUS ATIVADO 2: ', subAtivado[x]);
        console.log('X 2: ', x);

        document.getElementById(subAtivado[x].id).style.display = 'none';
        document.getElementById(subDesativado[x].id).style.display = 'block';
        pegaSelectSubAtivado.style.display = 'none';
        pegaSelectSubDesativado.style.display = 'block';
        sub[x].classList.remove('marcado' + idSub);
      }
    }
  }

  /**
   * MÉTODO PARA VERIFICAR OS ITENS MARCADOS
   * @param j 
   * @param idReq 
   * @param tipo 
   */
  verificaMarcados(j: any, idReq: any, tipo: any) {
    var marc = document.getElementById('requisito' + idReq);
    if (tipo == true && marc.classList[1] == 'marcado') {
      this.tamMarcado[j] = this.tamMarcado[j] + 1;
    }
    else {
      this.tamMarcado[j] = this.tamMarcado[j] - 1;
    }
  }

  /**
   * MÉTODO PARA PREENCHER OS CHECKBOX DAS SUBNORMAS, NESSE ELE VAI SELECIONAR SOMENTE QUANDO CLICAR NELE
   * @param idItem 
   * @param subnrId 
   * @param j 
   */
  async checarRequisito(idItem: string, subnrId: any, j: any) {
    let teste = document.getElementById('requisito' + idItem);
    console.log(teste.classList[1] + "   " + teste.classList[0]);
    this.contchamada = 0;
    if (teste.classList[1] != 'marcado' + subnrId) {
      document.getElementById('checkAtivadoItem' + subnrId + idItem).style.display = 'block';
      document.getElementById('checkNaoAtivadoItem' + subnrId + idItem).style.display = 'none';
      teste.classList.add('marcado' + subnrId);
      this.verificaMarcados(j, idItem, true);
    }
    else {
      document.getElementById('checkAtivadoItem' + subnrId + idItem).style.display = 'none';
      document.getElementById('checkNaoAtivadoItem' + subnrId + idItem).style.display = 'block';
      teste.classList.remove('marcado' + subnrId);
      this.verificaMarcados(j, idItem, false);
    }
  }

  /**
   * MÉTODO PARA FAZER O TRATAMENTO DOS ITENS QUE SERÃO MARCADOS
   * @param marcar 
   * @param index 
   * @param k 
   * @param subnrId 
   * @param valorSub 
   */
  editarMarcados(marcar, index, k, subnrId, valorSub) {
    if (this.contchamada != 0) {
      // var item = document.getElementById('requisito' + marcar).classList[2];
      var reqmarcados = [];
      //var checkar;
      var marcarSubTitulo = 0;
      if (document.getElementById('subNr' + subnrId).classList[3] != null) {
        this.contador = document.getElementById('subNr' + subnrId).classList[3];
      }
      var selectSubAtivado = document.getElementById('selectSubAtivado' + subnrId);
      var selectSubDesativado = document.getElementById('selectSubDesativado' + subnrId);
      var recebeAtivado = document.getElementById('checkAtivadoItem' + subnrId + marcar);
      var recebeDesativado = document.getElementById('checkNaoAtivadoItem' + subnrId + marcar);
      var itemMarc, marcarCheckbox;


      if (this.tamMarcado[valorSub] == null) {
        this.tamMarcado[valorSub] = 0;
      }
      var oleos = document.getElementsByClassName("marcado");
      for (var q = 0; q < oleos.length; q++) {
        console.log(oleos[q].classList[0]);
        if (oleos[q].classList[0] == 'subNr' + subnrId) {
          this.tamMarcado[valorSub]++;
        }
      }

      if (this.idmarcados[this.contador] != undefined && this.contchamada != 0) {
        for (var j = 0; j < this.idmarcados[this.contador].length; ++j) {
          itemMarc = 'requisito' + this.idmarcados[this.contador][j];
          marcarCheckbox = 'checkAtivadoItem' + subnrId + marcar;
          if (marcar == this.idmarcados[this.contador][j].trim()) {
            document.getElementById(itemMarc.trim()).classList.add('marcado');
            marcarSubTitulo++;
            console.log('oasldialshdlas', this.idmarcados);
            if (recebeAtivado != null) {
              recebeAtivado.style.display = 'block';
              console.log('ATI', recebeAtivado.classList.length);
            }
            if (recebeDesativado != null) {
              console.log('DESA', recebeDesativado.classList.length);
              recebeDesativado.style.display = 'none';
            }
          }
        }
      }
    }

    if (this.contchamada == 0) {
      var reqmarcados = [];
      this.contador = document.getElementById('subNr' + subnrId).classList[3];
      var marcados = document.getElementsByClassName('marcado' + subnrId);
      for (var i = 0; i < marcados.length; ++i) {
        reqmarcados[i] = marcados[i].innerHTML.trim();
      }
      this.idmarcados[this.contador] = reqmarcados;
      console.log(this.idmarcados);
    }
    //ADICIONANDO CLASSES
    if (recebeAtivado != null) {
      recebeAtivado.classList.add('subnrAtivado' + subnrId);
      recebeDesativado.classList.add('subnrDesativado' + subnrId);
    }
    if (marcarSubTitulo > 0 && selectSubAtivado != null) {
      selectSubAtivado.style.display = 'block';
      selectSubDesativado.style.display = 'none';
    }
    marcarSubTitulo = 0;
  }

  /**
   * MÉTODO PARA CONTROLAR O TEMPO PARA CHAMAR O MÉTODO EDITARMARCADOS, POIS ESTAVA APRESENTANDO ALGUNS BUGS
   * @param marcar 
   * @param index 
   * @param k 
   * @param subnrId 
   * @param j 
   */
  chamarMarcados(marcar, index, k, subnrId, j) {
    setTimeout(this.editarMarcados(marcar, index, k, subnrId, j), 300);
  }

  /**
   * MÉTODO PARA EXPANDIR AS NORMAS
   * @param item 
   * @param index 
   * @param numNr 
   */
  expandeNormas(item, index, idnr) {
    this.contchamada = 1;
    this.visual = 'auditoria2';
    this.storageProvider.getTodasSubnr(idnr)
      // this.serviceProvider.carregamentoPorDemandaSubnr(numNr)
      .then(resultado => {
        if (this.selectedDiag) {
          this.selectedDiag = 0;
        }
        else {
          this.selectedDiag = index;
        }
        console.log(resultado);
        console.log(document.getElementById('nr' + index).classList[2]);
        this.listarClickNormas = resultado;


        this.listaNormas.map((listItem) => {
          if (item == listItem) {
            listItem.expanded = !listItem.expanded;
          }
          else {
            listItem.expanded = false;
          }
          return listItem;
        });
        console.log('DEU CERTO');
      })
      .catch(erro => {
        let toast = this.toastCtrl.create({
          message: 'Não foi possível carregar as normas '+ erro,
          duration: 3000,
          cssClass: 'erro',
          position: 'bottom'
        })
        toast.present();
      });
  }


  /**
   * MÉTODO PARA EXPANDIR AS SUBNORMAS NO EXPAND
   * @param nr 
   * @param item 
   * @param indexsub 
   * @param indexNr 
   */
  expandeSubItem(item, indexNr, idSubnr, sub) {
    if (document.getElementById('subNr' + idSubnr).classList[2] != "clickado") {
      document.getElementById('subNr' + idSubnr).classList.add('clickado');
      document.getElementById('subNr' + idSubnr).classList.add(String(this.posicao));
      this.posicao++;
    }
    else {
      this.contador = document.getElementById('subNr' + idSubnr).classList[3];
    }
    this.storageProvider.getTodosRequisitos(idSubnr)
      .then(req => {
        if (this.selectedDiag) {
          this.selectedDiag = 0;
        }
        else {
          this.selectedDiag = indexNr;
        }

        this.listarRequisitos = req;

        sub.map((listSub) => {
          if (item == listSub) {
            listSub.expanded = !listSub.expanded;
            this.contchamada = 1;
          }
          else {
            listSub.expanded = false;
            this.contchamada = 0;
          }
          return listSub;
        });
      });
  }

  /**
   * MÉTODO PARA PARA ENVIAR PARA API O CADASTRO DA AUDITORIA
   */
  updateAuditoria() {
    this.serviceProvider.enviarAuditoria(this.navParams.get('dados'), this.navParams.get('dadosEmpresa'))
      .then(resultado => {
        if (resultado) {
          this.serviceProvider.enviaQuestoes(this.idmarcados, window.localStorage.getItem('idAuditoria'))
            .then(certo => {
              if (certo) {
                this.navCtrl.setRoot('AuditoriaTabPage');
              }
            })
            .catch(err => {
              let alert = this.alertCtrl.create({
                title: 'Dados não enviado',
                subTitle: 'Não foi possível enviar os dados.' + err,
                buttons: ['OK']
              });
              alert.present();
            })
        }
      })
      .catch(erro => {
        let alert = this.alertCtrl.create({
          title: 'Dados não enviado',
          subTitle: 'Não foi possível enviar os dados.' + erro,
          buttons: ['OK']
        });
        alert.present();
      })
  }
}