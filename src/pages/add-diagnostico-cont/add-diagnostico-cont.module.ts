import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddDiagnosticoContPage } from './add-diagnostico-cont';
import { ExpandeAuditoriaSubnormasComponent } from '../../components/expande-auditoria-subnormas/expande-auditoria-subnormas';
import { ExpandeAuditoriaNormasComponent } from '../../components/expande-auditoria-normas/expande-auditoria-normas';
import { ServiceProvider } from '../../providers/service/service';
import { StorageProvider } from '../../providers/storage/storage';
import { LoadingProvider } from '../../providers/loading/loading';

@NgModule({
  declarations: [
    AddDiagnosticoContPage,
    ExpandeAuditoriaNormasComponent,
    ExpandeAuditoriaSubnormasComponent
  ],
  imports: [
    IonicPageModule.forChild(AddDiagnosticoContPage)
  ],
  exports:[
    AddDiagnosticoContPage,
  ],
  providers: [
    ServiceProvider,
    StorageProvider,
    LoadingProvider
  ]

})
export class AddDiagnosticoContPageModule {}
