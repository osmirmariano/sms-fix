import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, Keyboard, Platform } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { ValueTransformer } from '@angular/compiler/src/util';
import { getValueFromFormat } from 'ionic-angular/util/datetime-util';
import * as papa from 'papaparse';
import { Http } from '@angular/http';
import { parse } from 'querystring';
import { SocialSharing } from '@ionic-native/social-sharing';
import { DatePicker } from '@ionic-native/date-picker';
import { File } from '@ionic-native/file';

@IonicPage()
@Component({
  selector: 'page-plano-acao',
  templateUrl: 'plano-acao.html',
})
export class PlanoAcaoPage {
  public planoAcao: any;
  public planoacao: any;
  public model; 
  public csvDados: any[] = [];
  public headerLinha: any[] = [];
  public contador: any = 0;
  public vetorResultado: any = Array();

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private formularioBuilder: FormBuilder,
    public http: Http,
    private alertCtrl: AlertController,
    public socialSharing: SocialSharing,
    public keyboard : Keyboard,
    public datePicker : DatePicker,
    private file: File,
    public platform: Platform) {
      this.planoacao = formularioBuilder.group({
        proposta: ['', Validators.compose([Validators.required])],
        responsavel: ['', Validators.compose([Validators.required])],
        prazoExecucao: ['', Validators.compose([Validators.required])],
        status: ['', Validators.compose([Validators.required])],
        item: [''],
        itemNormativo: [''],
        acaoPreventiva: [''],
        acaoCorretiva: [''],
        descricao: [''],
        infracao: [''],
        valorMin: [''],
        valorMax: ['']
      });
      this.model = new planoAcao();
  }

  ionViewDidLoad() {
    this.carregaPlanoAcao();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  carregaPlanoAcao(){
    var x, y, z;
    
    for(x = 0; x < this.navParams.get('planoacao').length; x++){
      for(y = 0; y < this.navParams.get('planoacao')[x].subNR.length; y++){
        for(z = 0; z < this.navParams.get('planoacao')[x].subNR[y].requisito.length; z++){
          if(this.navParams.get('planoacao')[x].subNR[y].requisito[z].resposta != 2){
            this.vetorResultado.push({
              item: this.navParams.get('planoacao')[x].NumNR,
              itemNormativo: this.navParams.get('planoacao')[x].subNR[y].numero,
              descricao: this.navParams.get('planoacao')[x].subNR[y].requisito[z].descricao,
            })
          }
        }
      }
    }
    console.log('NOVO VETOR: ', this.vetorResultado);
  }

  update(value, x){
    this.model.item[x] = value;
  }

  update2(value, x){
    this.model.itemNormativo[x] = value;
  }

  update3(value, x){
    this.model.descricao[x] = value;
  }

  update4(value, x){
    this.model.infracao[x] = value;
  }
  
  update5(value, x){
    this.model.valorMin[x] = value;
  }

  update6(value, x){
    this.model.valorMax[x] = value;
  }

  gerarPlano(){
    var novoVetor = [], x;
    if(this.planoacao){
      for (x = 0; x < this.model.acaoCorretiva.length; x++){
        novoVetor.push({
          item: this.model.item[x],
          itemNormativo: this.model.itemNormativo[x],
          acaoCorretiva: this.model.acaoCorretiva[x],
          acaoPreventiva: this.model.acaoPreventiva[x],
          descricao: this.model.descricao[x],
          infracao: this.model.infracao[x],
          prazoExecucao: this.model.prazoExecucao[x],
          proposta: this.model.proposta[x],
          responsavel: this.model.responsavel[x],
          status: this.model.status[x],
          valorMax: this.model.valorMax[x],
          valorMin: this.model.valorMin[x]
        })
      }
      this.salvarExcel(novoVetor);
    }
    else{
      let alert = this.alertCtrl.create({
        title: 'Campos obrigatórios',
        subTitle: 'Por favor, preencha todos os campos',
        buttons: ['OK']
      });
      alert.present();
    }
    console.log('MODEL FINAL', this.model);
  }

  salvarExcel(jsonValor){
    console.log(jsonValor);
    let dados = JSON.stringify(jsonValor);
    let sampleJson = JSON.parse(dados);
    let path = null;
    let csvData = this.converterForExcel(sampleJson);    
    var arquivoCsv = "smsfix.csv";

    /*
    //Código aqui para baixar arquivo
    this.file.writeExistingFile(this.file.externalRootDirectory, arquivoCsv, csvData)
    .then(dados =>{
      if(this.platform.is('android')){
        path = this.file.dataDirectory;
        console.log('ANDROID: ', path);
      }
      else if(this.platform.is('ios')){
        path = this.file.documentsDirectory;
        console.log('IOS: ', path);
      }
      console.log('DEU CERTO, PRIMEIRO CASO');
    })
    .catch(erro => {
      this.file.writeExistingFile(this.file.externalRootDirectory, arquivoCsv, csvData)
      .then(succes => {
        console.log('DEU CERTO, SEGUNDO CASO');
      })
      .catch(err =>{
        console.log('DEU TUDO ERRADO');
      })
    })
    */
    
    let blob = new Blob([csvData], { type: 'text/csv' });   
    let a = window.document.createElement("a");  
    a.href = window.URL.createObjectURL(blob);
    a.download = 'smsfix.csv'; 
    document.body.appendChild(a);    
    a.click();
    document.body.removeChild(a);
      
    // this.compartilhar();
  }

  compartilhar(){
    this.socialSharing.share("Plano de ação.", null, "file:///data/user/0/io.ionic.starter/files/smsfix.csv", null);
  }

  converterForExcel(paraConverter){
    let array = typeof paraConverter != 'object' ? JSON.parse(paraConverter) : paraConverter;
    let str = '';
    let row = ""; 
    for (let index in paraConverter[0]) {
      row += index + ';';
    }
    row = row.slice(0, -1);
    str += row + '\r\n';
    for (let i = 0; i < array.length; i++) {
      let line = '';
      for (let index in array[i]) {
        if (line != '') line += ';';
        line += array[i][index];
      }
      str += line + '\r\n';
    }
    return str;
  }

  openDatepicker(index){
    this.keyboard.close();
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
    }).then(
      date => {
        this.model.prazoExecucao[index] = date.getDate()+'/'+date.getMonth()+'/'+date.getFullYear()},
      err => console.log('Erro ao processar a data: ', err)
    );
  }
}


class planoAcao{
  proposta: any = Array();
  responsavel: any = Array();
  prazoExecucao: any = Array();
  acaoCorretiva: any = Array();
  acaoPreventiva: any = Array();
  status: any = Array();
  descricao: any = Array();
  infracao: any = Array();
  valorMin: any = Array()
  valorMax: any = Array();
  item: any = Array();
  itemNormativo: any = Array();
}