import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlanoAcaoPage } from './plano-acao';
import { SocialSharing } from '@ionic-native/social-sharing';
import { File } from '@ionic-native/file';

@NgModule({
  declarations: [
    PlanoAcaoPage,
  ],
  imports: [
    IonicPageModule.forChild(PlanoAcaoPage),
  ],
  exports: [
    PlanoAcaoPage
  ],
  providers: [
    SocialSharing,
    File
  ]
})
export class PlanoAcaoPageModule {}
