import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform, ToastController } from 'ionic-angular';
import { Validators, FormBuilder } from '@angular/forms';
import { EmailValidator } from '../../validators/email';
import { Users } from '../../models/users';
import { ServiceProvider } from '../../providers/service/service';

@IonicPage()
@Component({
  selector: 'page-criar-conta',
  templateUrl: 'criar-conta.html',
})
export class CriarContaPage {

  model: Users;
  public loginForm;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private formularioBuilder: FormBuilder,
    public platform: Platform,
    private toast: ToastController,
    private serviceProvider: ServiceProvider) {

      this.loginForm = formularioBuilder.group({
        email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
        senha: ['', Validators.compose([Validators.minLength(8), Validators.required])],
        nome: ['', Validators.compose([Validators.required])],
        dd: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(2), Validators.required])],
        telefone: ['', Validators.compose([Validators.required])],
        aceita_termos: ['', Validators.compose([Validators.required])],
      });
  
      this.model = new Users();
  }

  ionViewDidLoad() { }

  voltarLogin(){
    this.navCtrl.push('LoginPage');
  }

  termoUso(url){
    this.platform.ready()
    .then(() => {
      open(url, "_blank", "location=no");
    });
  }

  politicaPrivacidade(url){
    this.platform.ready()
      .then(() => {
        open(url, "_blank", "location=no");
      });
  }

  criarConta(){
    if (this.model.aceita_termos == null || this.model.aceita_termos == false){
      this.toast.create({
        message: 'Para criar sua conta é necessário que você aceite os termos de uso do aplicativo.',
        cssClass :'termos',
        position: 'bottom',
        duration: 1000
      }).present();
    }
    else{
      if (!this.loginForm.valid) {
        this.toast.create({
          message: 'Por favor, verifique se todos campos estão corretos.',
          position: 'bottom',
          cssClass: 'usuario-criado',
          duration: 3000})
        .present();
        console.log(this.loginForm.value);
      }
      else{
        this.serviceProvider.criarConta(this.model.nome, this.model.email, this.model.senha, this.model.dd, this.model.telefone, this.model.aceita_termos)
        .then(resultado => {
          if(resultado == 0){
            this.navCtrl.setRoot('LoginPage');
            this.toast.create({
              message: 'Cadastro realizado com sucesso.',
              position: 'bottom',
              cssClass: 'usuario-criado',
              duration: 1000})
            .present();
          }
          else{
            this.toast.create({
              message: 'E-mail já cadastrado.',
              position: 'bottom',
              cssClass: 'usuario-criado',
              duration: 1000})
            .present();
          }
        })
      }
    }
  }
}
