import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CriarContaPage } from './criar-conta';
import { ServiceProvider } from '../../providers/service/service';

@NgModule({
  declarations: [
    CriarContaPage,
  ],
  imports: [
    IonicPageModule.forChild(CriarContaPage),
  ],
  exports: [
    CriarContaPage
  ],
  providers: [
    ServiceProvider
  ]
})
export class CriarContaPageModule {}
