import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AndamentoPage } from './andamento';
import { ServiceProvider } from '../../providers/service/service';
import { ExpandeAndamentoComponent } from '../../components/expande-andamento/expande-andamento';
import { File } from '@ionic-native/file'
import { LocalNotifications } from '@ionic-native/local-notifications';
import { FileTransfer } from '@ionic-native/file-transfer';
import { FileOpener } from '@ionic-native/file-opener';

@NgModule({
  declarations: [
    AndamentoPage,
    ExpandeAndamentoComponent
  ],
  imports: [
    IonicPageModule.forChild(AndamentoPage),
  ],
  providers: [
    ServiceProvider,
    File,
    LocalNotifications,
    FileTransfer,
    FileOpener
  ]
})
export class AndamentoPageModule {}
