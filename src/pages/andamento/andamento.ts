import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, ModalController, AlertController, Platform, ToastController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { PopoverComponent } from '../../components/popover/popover';
import { File } from '@ionic-native/file'
import { FileOpener } from '@ionic-native/file-opener';
import * as pdfmake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { StorageProvider } from '../../providers/storage/storage';
import { FileTransfer } from '@ionic-native/file-transfer';


@IonicPage()
@Component({
  selector: 'page-andamento',
  templateUrl: 'andamento.html',
})
export class AndamentoPage {
  selectedDiag: any;
  listaCliente: any = Array();
  listaMostra: any = Array();
  public idAuditoria: any;
  public graficoResultados: any;
  public maximo: any;
  public minimo: any;
  public numFuncionario: any;
  public tipoAuditoria: any;
  public listaCalculo: any;
  public popover: any;
  public listaNrs: any;
  public testRadioOpen: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    private modalCtrl: ModalController,
    public serviceProvider: ServiceProvider,
    private alertCtrl: AlertController,
    public platform: Platform,
    private file: File,
    private fileOpener: FileOpener,
    private storageProvider: StorageProvider,
    private transfer: FileTransfer,
    private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    this.carregaAuditoria();
    this.carregaNrs();
  }

  /*
   * MÉTODO PARA CONFIGURAR O BOTÃO DO POPOVER
  */
  botaoVoltar(){
    this.platform.ready()
    .then(() => {
      this.platform.registerBackButtonAction((event) => {
        if(this.navCtrl.getActive().name === 'AndamentoPage'){
          this.popover.dismiss();
        }
      })
    })
  }

  /*
   * MÉTODO PARA CHAMAR A PAGE PARA CRIAR AUDITORIA
  */
  adicionarAuditoria(){
    let modal = this.modalCtrl.create('AddDiagnosticoPage');
    modal.present();
  }

  /**
   * MÉTODO PARA CARREGAR TODAS AS AUDITORIAS CADASTRADAS
  */
  carregaAuditoria() {
    this.serviceProvider.auditoria()
      .then(data => {
        if (data) {
          let dados = JSON.stringify(data);
          this.listaCliente = JSON.parse(dados).clientes;
          console.log('RESULTADO CLIENTES: ', this.listaCliente);
        }
      })
      .catch(erro => {
        console.log('DEU ERRO');
      })
  }

  /**
   * MÉTODO PARA CARREGAR SOBRE DEMANDAS AS NORMAS
   */
  carregaNrs(){
    // this.serviceProvider.carregamentoPorDemandaNR()
    this.storageProvider.getTodasNr()
    .then(data => {
      if (data) {
        this.listaNrs = data;
        window.localStorage.setItem('nr', JSON.stringify(data));
        console.log(data);
      }
    })
    .catch(erro => {
      console.log('DEU ERRO');
    })
  }

  /**
   * MÉTODO PARA EXPANDIR AS NORMAS DO COMPONENT EXPAND
   * @param item 
   * @param index 
   */
  expandeAuditoria(item, index) {
    if (this.selectedDiag) {
      this.selectedDiag = 0;
    }
    else {
      this.selectedDiag = index;
    }

    this.listaCliente.map((listItem) => {
      if (item == listItem) {
        listItem.expanded = !listItem.expanded;
      }
      else {
        listItem.expanded = false;
      }
      return listItem;
    });
  }

  /**
   * MÉTODO PARA RETORNAR OS DADOS DA AUDITORIA DE ACORDO COM SEU ID
   */
  carregarGrafico() {
    this.serviceProvider.paraGrafico(this.idAuditoria)
      .then(resultado => {
        this.graficoResultados = resultado;
        window.localStorage.setItem('grafico', JSON.stringify(resultado));
      })
      .catch(erro => {
        console.log('DEU ERRO');
      })
  }

  /**
    * MÉTODO PARA RECEBER A ESCOLHA DO MODAL DO POPOVER E REALIZAR A OPERAÇÃO DE ACORDO
    * @param myEvent 
    * @param dados 
    * @param x 
    * @param auditoria 
  */
  presentPopover(myEvent, dados, x, auditoria) {
    console.log('EVENTO: ', auditoria);
    this.idAuditoria = auditoria.idAuditoria;
    if (auditoria.reqAuditoria.length != 0){
      this.carregarGrafico();
    }
    
    this.popover = this.popoverCtrl.create(PopoverComponent, { cssClass: 'popover' });
    this.popover.present({
      ev: myEvent
    });
    
    this.popover.onDidDismiss(popoverData => {
      if (popoverData == null) {
        console.log('NULL');
        console.log('NOVO: ', this.popover);
      }
      else {
        if (popoverData.item == "Editar") {
          let modal = this.modalCtrl.create('EditarPage', { selecionado: dados });
          modal.present();
        }
        else {
          if (popoverData.item == "Coleta de dados") {
            let modal = this.modalCtrl.create('ColetaDadosAuditoriasPage', { index: x,  audSelecionado: this.graficoResultados});
            modal.present();
          }
          else {
            if (popoverData.item == "Visualizar") {
              let modal = this.modalCtrl.create('VisualizarPage', { selecionado: dados, auditoria: auditoria });
              modal.present();
            }
            else {
              if (popoverData.item == "Gráfico") {
                if (auditoria.reqAuditoria.length == 0) {
                  let alert = this.alertCtrl.create({
                    title: 'Auditoria não registrada',
                    subTitle: 'Não é possível gerar o gráfico, pois ainda não foi realizada a coleta de dados da auditoria.',
                    buttons: ['OK']
                  });
                  alert.present();
                }
                else {
                  let modal = this.modalCtrl.create('GraficoPage', { grafico: this.idAuditoria, objetoGrafico: this.graficoResultados });
                  modal.present();
                }
              }
              else {
                if (popoverData.item == "Plano de ação") {
                  console.log('AD: ', auditoria);
                  console.log('AD 2: ', auditoria.descricao);
                  this.numFuncionario = auditoria.NFuncionarios;
                  if(auditoria.tipo == 1){
                    this.tipoAuditoria = 's';
                  }
                  else{
                    this.tipoAuditoria = 'm'; 
                  }
                  
                  if (auditoria.reqAuditoria.length == 0) {
                    let alert = this.alertCtrl.create({
                      title: 'Auditoria não registrada',
                      subTitle: 'Não é possível gerar o plano de ação, pois ainda não foi realizada a coleta de dados da auditoria.',
                      buttons: ['OK']
                    });
                    alert.present();
                  }
                  else {
                    let x, y, z, t, contador1 = 0, descricao, totalMax = 0, totalMin = 0;;
                    var vetorResultado = [];
                    
                    this.serviceProvider.listarValores()
                    .then(resultado => {
                      var tratamento = JSON.stringify(resultado);
                      this.listaCalculo = tratamento;
                      for(t = 0; t < this.listaNrs.length; t++){
                        for(x = 0; x < this.graficoResultados.length; x++){
                          if(this.listaNrs[t].idNR == this.graficoResultados[x].nr_NumNR){
                          for(y = 0; y < this.graficoResultados[x].requisito.length; y++){
                            if(this.graficoResultados[x].requisito[y].resposta != 2){
                              var infracaoCalculo = this.graficoResultados[x].requisito[y].infracao;
                                for(let a = 0; a < JSON.parse(this.listaCalculo).length; a++){
                                  if(this.graficoResultados[x].requisito[y].descricao == null)
                                    descricao = "";
                                  else
                                    descricao = this.graficoResultados[x].requisito[y].descricao;
                                  if(this.numFuncionario <= 10){
                                    if(this.tipoAuditoria == 's'){
                                      if(JSON.parse(this.listaCalculo)[a].NInfracao == infracaoCalculo){
                                        this.maximo = JSON.parse(this.listaCalculo)[a].maximoSeg*(1.0641);
                                        this.minimo = JSON.parse(this.listaCalculo)[a].minimoSeg*(1.0641);
                                        totalMax = totalMax + this.maximo;
                                        totalMin = totalMin + this.minimo;
                                        vetorResultado.push({
                                          NR: this.listaNrs[t].NumNR,
                                          itemNor: this.graficoResultados[x].numero,
                                          Descricao: descricao,
                                          Proposta: '',
                                          Responsavel: '',
                                          Prazo_Execucao: '',
                                          Acao_Corretiva: '',
                                          Acao_Preventiva: '',
                                          Infracao: infracaoCalculo,
                                          val_Min: this.minimo.toFixed(2),
                                          val_Max: this.maximo.toFixed(2),
                                          Status: '',
                                        })
                                      }
                                    }
                                    else{
                                      if(JSON.parse(this.listaCalculo)[a].NInfracao == infracaoCalculo){
                                        this.maximo = JSON.parse(this.listaCalculo)[a].maximoMed*(1.0641);
                                        this.minimo = JSON.parse(this.listaCalculo)[a].minimoMed*(1.0641);
                                        totalMax = totalMax + this.maximo;
                                        totalMin = totalMin + this.minimo;
                                        vetorResultado.push({
                                          NR: this.listaNrs[t].NumNR,
                                          itemNor: this.graficoResultados[x].numero,
                                          Descricao: descricao,
                                          Proposta: '',
                                          Responsavel: '',
                                          Prazo_Execucao: '',
                                          Acao_Corretiva: '',
                                          Acao_Preventiva: '',
                                          Infracao: infracaoCalculo,
                                          val_Min: this.minimo.toFixed(2),
                                          val_Max: this.maximo.toFixed(2),
                                          Status: '',
                                        })
                                      }
                                    }
                                    console.log('< 10');
                                  }

                                  else if(this.numFuncionario >= 11 || this.numFuncionario <= 25){
                                    if(this.tipoAuditoria == 's'){
                                      if(JSON.parse(this.listaCalculo)[a].NInfracao == infracaoCalculo){
                                        this.maximo = JSON.parse(this.listaCalculo)[a].maximoSeg*(1.0641);
                                        this.minimo = JSON.parse(this.listaCalculo)[a].minimoSeg*(1.0641);
                                        totalMax = totalMax + this.maximo;
                                        totalMin = totalMin + this.minimo;
                                        vetorResultado.push({
                                          NR: this.listaNrs[t].NumNR,
                                          itemNor: this.graficoResultados[x].numero,
                                          Descricao: descricao,
                                          Proposta: '',
                                          Responsavel: '',
                                          Prazo_Execucao: '',
                                          Acao_Corretiva: '',
                                          Acao_Preventiva: '',
                                          Infracao: infracaoCalculo,
                                          val_Min: this.minimo.toFixed(2),
                                          val_Max: this.maximo.toFixed(2),
                                          Status: '',
                                        })
                                      }
                                    }
                                    else{
                                      if(JSON.parse(this.listaCalculo)[a].NInfracao == infracaoCalculo){
                                        this.maximo = JSON.parse(this.listaCalculo)[a].maximoMed*(1.0641);
                                        this.minimo = JSON.parse(this.listaCalculo)[a].minimoMed*(1.0641);
                                        totalMax = totalMax + this.maximo;
                                        totalMin = totalMin + this.minimo;
                                        vetorResultado.push({
                                          NR: this.listaNrs[t].NumNR,
                                          itemNor: this.graficoResultados[x].numero,
                                          Descricao: descricao,
                                          Proposta: '',
                                          Responsavel: '',
                                          Prazo_Execucao: '',
                                          Acao_Corretiva: '',
                                          Acao_Preventiva: '',
                                          Infracao: infracaoCalculo,
                                          val_Min: this.minimo.toFixed(2),
                                          val_Max: this.maximo.toFixed(2),
                                          Status: '',
                                        })
                                      }
                                    }
                                    console.log('> 10 && < 25');
                                  }

                                  else if(this.numFuncionario >= 26 || this.numFuncionario <= 50){
                                    if(this.tipoAuditoria == 's'){
                                      if(JSON.parse(this.listaCalculo)[a].NInfracao == infracaoCalculo){
                                        this.maximo = JSON.parse(this.listaCalculo)[a].maximoSeg*(1.0641);
                                        this.minimo = JSON.parse(this.listaCalculo)[a].minimoSeg*(1.0641);
                                        totalMax = totalMax + this.maximo;
                                        totalMin = totalMin + this.minimo;
                                        vetorResultado.push({
                                          NR: this.listaNrs[t].NumNR,
                                          itemNor: this.graficoResultados[x].numero,
                                          Descricao: descricao,
                                          Proposta: '',
                                          Responsavel: '',
                                          Prazo_Execucao: '',
                                          Acao_Corretiva: '',
                                          Acao_Preventiva: '',
                                          Infracao: infracaoCalculo,
                                          val_Min: this.minimo.toFixed(2),
                                          val_Max: this.maximo.toFixed(2),
                                          Status: '',
                                        })
                                      }
                                    }
                                    else{
                                      if(JSON.parse(this.listaCalculo)[a].NInfracao == infracaoCalculo){
                                        this.maximo = JSON.parse(this.listaCalculo)[a].maximoMed*(1.0641);
                                        this.minimo = JSON.parse(this.listaCalculo)[a].minimoMed*(1.0641);
                                        totalMax = totalMax + this.maximo;
                                        totalMin = totalMin + this.minimo;
                                        vetorResultado.push({
                                          NR: this.listaNrs[t].NumNR,
                                          itemNor: this.graficoResultados[x].numero,
                                          Descricao: descricao,
                                          Proposta: '',
                                          Responsavel: '',
                                          Prazo_Execucao: '',
                                          Acao_Corretiva: '',
                                          Acao_Preventiva: '',
                                          Infracao: infracaoCalculo,
                                          val_Min: this.minimo.toFixed(2),
                                          val_Max: this.maximo.toFixed(2),
                                          Status: '',
                                        })
                                      }
                                    }
                                    console.log('> 25 && < 50');
                                  }

                                  else if(this.numFuncionario >= 51 || this.numFuncionario <= 100){
                                    if(this.tipoAuditoria == 's'){
                                      if(JSON.parse(this.listaCalculo)[a].NInfracao == infracaoCalculo){
                                        this.maximo = JSON.parse(this.listaCalculo)[a].maximoSeg*(1.0641);
                                        this.minimo = JSON.parse(this.listaCalculo)[a].minimoSeg*(1.0641);
                                        totalMax = totalMax + this.maximo;
                                        totalMin = totalMin + this.minimo;
                                        vetorResultado.push({
                                          NR: this.listaNrs[t].NumNR,
                                          itemNor: this.graficoResultados[x].numero,
                                          Descricao: descricao,
                                          Proposta: '',
                                          Responsavel: '',
                                          Prazo_Execucao: '',
                                          Acao_Corretiva: '',
                                          Acao_Preventiva: '',
                                          Infracao: infracaoCalculo,
                                          val_Min: this.minimo.toFixed(2),
                                          val_Max: this.maximo.toFixed(2),
                                          Status: '',
                                        })
                                      }
                                    }
                                    else{
                                      if(JSON.parse(this.listaCalculo)[a].NInfracao == infracaoCalculo){
                                        this.maximo = JSON.parse(this.listaCalculo)[a].maximoMed*(1.0641);
                                        this.minimo = JSON.parse(this.listaCalculo)[a].minimoMed*(1.0641);
                                        totalMax = totalMax + this.maximo;
                                        totalMin = totalMin + this.minimo;
                                        vetorResultado.push({
                                          NR: this.listaNrs[t].NumNR,
                                          itemNor: this.graficoResultados[x].numero,
                                          Descricao: descricao,
                                          Proposta: '',
                                          Responsavel: '',
                                          Prazo_Execucao: '',
                                          Acao_Corretiva: '',
                                          Acao_Preventiva: '',
                                          Infracao: infracaoCalculo,
                                          val_Min: this.minimo.toFixed(2),
                                          val_Max: this.maximo.toFixed(2),
                                          Status: '',
                                        })
                                      }
                                    }
                                    console.log('> 50 && < 100');
                                  }

                                  else if(this.numFuncionario >= 101 || this.numFuncionario <= 250){
                                    if(this.tipoAuditoria == 's'){
                                      if(JSON.parse(this.listaCalculo)[a].NInfracao == infracaoCalculo){
                                        this.maximo = JSON.parse(this.listaCalculo)[a].maximoSeg*(1.0641);
                                        this.minimo = JSON.parse(this.listaCalculo)[a].minimoSeg*(1.0641);
                                        totalMax = totalMax + this.maximo;
                                        totalMin = totalMin + this.minimo;
                                        vetorResultado.push({
                                          NR: this.listaNrs[t].NumNR,
                                          itemNor: this.graficoResultados[x].numero,
                                          Descricao: descricao,
                                          Proposta: '',
                                          Responsavel: '',
                                          Prazo_Execucao: '',
                                          Acao_Corretiva: '',
                                          Acao_Preventiva: '',
                                          Infracao: infracaoCalculo,
                                          val_Min: this.minimo.toFixed(2),
                                          val_Max: this.maximo.toFixed(2),
                                          Status: '',
                                        })
                                      }
                                    }
                                    else{
                                      if(JSON.parse(this.listaCalculo)[a].NInfracao == infracaoCalculo){
                                        this.maximo = JSON.parse(this.listaCalculo)[a].maximoMed*(1.0641);
                                        this.minimo = JSON.parse(this.listaCalculo)[a].minimoMed*(1.0641);
                                        totalMax = totalMax + this.maximo;
                                        totalMin = totalMin + this.minimo;
                                        vetorResultado.push({
                                          NR: this.listaNrs[t].NumNR,
                                          itemNor: this.graficoResultados[x].numero,
                                          Descricao: descricao,
                                          Proposta: '',
                                          Responsavel: '',
                                          Prazo_Execucao: '',
                                          Acao_Corretiva: '',
                                          Acao_Preventiva: '',
                                          Infracao: infracaoCalculo,
                                          val_Min: this.minimo.toFixed(2),
                                          val_Max: this.maximo.toFixed(2),
                                          Status: '',
                                        })
                                      }
                                    }
                                    console.log('> 100 && < 250'); 
                                  }

                                  else if(this.numFuncionario >= 251 ||this.numFuncionario <= 500){
                                    if(this.tipoAuditoria == 's'){
                                      if(JSON.parse(this.listaCalculo)[a].NInfracao == infracaoCalculo){
                                        this.maximo = JSON.parse(this.listaCalculo)[a].maximoSeg*(1.0641);
                                        this.minimo = JSON.parse(this.listaCalculo)[a].minimoSeg*(1.0641);
                                        totalMax = totalMax + this.maximo;
                                        totalMin = totalMin + this.minimo;
                                        vetorResultado.push({
                                          NR: this.listaNrs[t].NumNR,
                                          itemNor: this.graficoResultados[x].numero,
                                          Descricao: descricao,
                                          Proposta: '',
                                          Responsavel: '',
                                          Prazo_Execucao: '',
                                          Acao_Corretiva: '',
                                          Acao_Preventiva: '',
                                          Infracao: infracaoCalculo,
                                          val_Min: this.minimo.toFixed(2),
                                          val_Max: this.maximo.toFixed(2),
                                          Status: '',
                                        })
                                      }
                                    }
                                    else{
                                      if(JSON.parse(this.listaCalculo)[a].NInfracao == infracaoCalculo){
                                        this.maximo = JSON.parse(this.listaCalculo)[a].maximoMed*(1.0641);
                                        this.minimo = JSON.parse(this.listaCalculo)[a].minimoMed*(1.0641);
                                        totalMax = totalMax + this.maximo;
                                        totalMin = totalMin + this.minimo;
                                        vetorResultado.push({
                                          NR: this.listaNrs[t].NumNR,
                                          itemNor: this.graficoResultados[x].numero,
                                          Descricao: descricao,
                                          Proposta: '',
                                          Responsavel: '',
                                          Prazo_Execucao: '',
                                          Acao_Corretiva: '',
                                          Acao_Preventiva: '',
                                          Infracao: infracaoCalculo,
                                          val_Min: this.minimo.toFixed(2),
                                          val_Max: this.maximo.toFixed(2),
                                          Status: '',
                                        })
                                      }
                                    }
                                    console.log('> 250 && < 500'); 
                                  }

                                  else if(this.numFuncionario >= 501 || this.numFuncionario <= 1000){
                                    if(this.tipoAuditoria == 's'){
                                      if(JSON.parse(this.listaCalculo)[a].NInfracao == infracaoCalculo){
                                        this.maximo = JSON.parse(this.listaCalculo)[a].maximoSeg*(1.0641);
                                        this.minimo = JSON.parse(this.listaCalculo)[a].minimoSeg*(1.0641);
                                        totalMax = totalMax + this.maximo;
                                        totalMin = totalMin + this.minimo;
                                        vetorResultado.push({
                                          NR: this.listaNrs[t].NumNR,
                                          itemNor: this.graficoResultados[x].numero,
                                          Descricao: descricao,
                                          Proposta: '',
                                          Responsavel: '',
                                          Prazo_Execucao: '',
                                          Acao_Corretiva: '',
                                          Acao_Preventiva: '',
                                          Infracao: infracaoCalculo,
                                          val_Min: this.minimo.toFixed(2),
                                          val_Max: this.maximo.toFixed(2),
                                          Status: '',
                                        })
                                      }
                                    }
                                    else{
                                      if(JSON.parse(this.listaCalculo)[a].NInfracao == infracaoCalculo){
                                        this.maximo = JSON.parse(this.listaCalculo)[a].maximoMed*(1.0641);
                                        this.minimo = JSON.parse(this.listaCalculo)[a].minimoMed*(1.0641);
                                        totalMax = totalMax + this.maximo;
                                        totalMin = totalMin + this.minimo;
                                        vetorResultado.push({
                                          NR: this.listaNrs[t].NumNR,
                                          itemNor: this.graficoResultados[x].numero,
                                          Descricao: descricao,
                                          Proposta: '',
                                          Responsavel: '',
                                          Prazo_Execucao: '',
                                          Acao_Corretiva: '',
                                          Acao_Preventiva: '',
                                          Infracao: infracaoCalculo,
                                          val_Min: this.minimo.toFixed(2),
                                          val_Max: this.maximo.toFixed(2),
                                          Status: '',
                                        })
                                      }
                                    }
                                    console.log('> 500 && < 1000');
                                  }

                                  else if(this.numFuncionario > 1000){
                                    if(this.tipoAuditoria == 's'){
                                      if(JSON.parse(this.listaCalculo)[a].NInfracao == infracaoCalculo){
                                        this.maximo = JSON.parse(this.listaCalculo)[a].maximoSeg*(1.0641);
                                        this.minimo = JSON.parse(this.listaCalculo)[a].minimoSeg*(1.0641);
                                        totalMax = totalMax + this.maximo;
                                        totalMin = totalMin + this.minimo;
                                        vetorResultado.push({
                                          NR: this.listaNrs[t].NumNR,
                                          itemNor: this.graficoResultados[x].numero,
                                          Descricao: descricao,
                                          Proposta: '',
                                          Responsavel: '',
                                          Prazo_Execucao: '',
                                          Acao_Corretiva: '',
                                          Acao_Preventiva: '',
                                          Infracao: infracaoCalculo,
                                          val_Min: this.minimo.toFixed(2),
                                          val_Max: this.maximo.toFixed(2),
                                          Status: '',
                                        })
                                      }
                                    }
                                    else{
                                      if(JSON.parse(this.listaCalculo)[a].NInfracao == infracaoCalculo){
                                        this.maximo = JSON.parse(this.listaCalculo)[a].maximoMed*(1.0641);
                                        this.minimo = JSON.parse(this.listaCalculo)[a].minimoMed*(1.0641);
                                        totalMax = totalMax + this.maximo;
                                        totalMin = totalMin + this.minimo;
                                        vetorResultado.push({
                                          NR: this.listaNrs[t].NumNR,
                                          itemNor: this.graficoResultados[x].numero,
                                          Descricao: descricao,
                                          Proposta: '',
                                          Responsavel: '',
                                          Prazo_Execucao: '',
                                          Acao_Corretiva: '',
                                          Acao_Preventiva: '',
                                          Infracao: infracaoCalculo,
                                          val_Min: this.minimo.toFixed(2),
                                          val_Max: this.maximo.toFixed(2),
                                          Status: '',
                                        })
                                      }
                                    }
                                    console.log('Acima de 1000');
                                  }
                                }
                                contador1 = 1;
                              }
                            }
                          }
                        }
                      }
                      console.log('TEST: ', vetorResultado);
                      if(contador1 == 0){
                        let alert = this.alertCtrl.create({
                          title: 'Plano de ação',
                          subTitle: 'Não será gerado plano, pois todos os requisitos atendem a NR.',
                          buttons: ['OK']
                        });
                        alert.present();
                      }
                      console.log('NOVO VETOR: ', vetorResultado);
                      this.alertEscolha(vetorResultado, dados, auditoria.parecerTec, auditoria.PCritico, totalMax, totalMin, auditoria.descricao);
                    })
                  }
                }
                else{
                  if(popoverData.item == "Finalizar"){
                    console.log('FINALIZAR');
                  }
                  else{
                    if(popoverData.item == "Excluir"){
                      console.log('EXCLUIR');
                    }
                  }
                }
              }
            }
          }
        }
      }
      console.log(popoverData);
    })
  }

  /**
    * MÉTODO PARA DEFINIR E ESCOLHER O QUAL TIPO DE ARQUIVO IRÁ GERAR DO PLANO DE AÇÃO
    * @param vetorResultado 
    * @param dados 
    * @param parecerTec 
    * @param pontoCri 
  */
  alertEscolha(vetorResultado, dados, parecerTec, pontoCri, maxTotal, minTotal, descricao){
    let confirm = this.alertCtrl.create({
      title: 'Plano de ação',
      message: 'Escolha a extensão para gerar o plano de ação. Recomendamos que baixa o Google Planilha e um visualizador de PDF para abrir os arquivos gerados.',
      buttons: [
        {
          text: 'Baixar em CSV',
          handler: () => {
            this.salvarExcel(vetorResultado);
          }
        },
        {
          text: 'Baixar em PDF',
          handler: () => {
            this.createPdf(vetorResultado, dados, parecerTec, pontoCri, maxTotal, minTotal, descricao);
          }
        }
      ]
    });
    confirm.present();
  }

  
  /**
   * MÉTODO PARA CONVERTER O OBJETO JSON EM CSV
   * @param paraConverter 
   */
  converterForExcel(paraConverter){
    let array = typeof paraConverter != 'object' ? JSON.parse(paraConverter) : paraConverter;
    let str = '';
    let row = ""; 
    for (let index in paraConverter[0]) {
      row += index + ',';
    }
    row = row.slice(0, -1);
    str += row + '\r\n';
    for (let i = 0; i < array.length; i++) {
      let line = '';
      for (let index in array[i]) {
        if (line != '') line += ',';
        line += array[i][index];
      }
      str += line + '\r\n';
    }
    return str;
  }

  /**
   * MÉTODO PARA CRIAR PDF
   * @param vetor 
   * @param dadosEmpresa 
   * @param parTecnico 
   * @param parCritico 
   */
  createPdf(vetor, dadosEmpresa, parTecnico, parCritico, maxTotal, minTotal, descricao) {
    var data = new Date();
    var dia = data.getDate();
    var mes = data.getMonth() + 1;
    var ano = data.getFullYear();
    var horas = new Date().getHours();
    var minutos = new Date().getMinutes();
    var segundos = new Date().getSeconds();
    var resultadoData = dia + "/" + mes + "/" + ano;
    var resultadoHora = horas + "h:" + minutos + "min:" + segundos + "s";

    let self = this;
    pdfmake.vfs = pdfFonts.pdfMake.vfs;
    var docDefinition = {
      
      content: [
        {
          columns: [
            { text: descricao + '\n' + dadosEmpresa.nomeClientes + ' - ' + dadosEmpresa.cnpj, style: 'nomeDescricao'  },
            { text: 'SMSFIX', style: 'image' }
          ]
        },
        // { text: descricao, style: 'nomeDescricao' },
        // { text: dadosEmpresa.nomeClientes + ' - ' + dadosEmpresa.cnpj, style: 'header' },
        // { text: 'SMSFIX', style: 'image' },
        { text: 'Data: ' + resultadoData + ' - ' + 'Hora: ' + resultadoHora, style: ['data'] },
        { text: '\n'},
        { text: 'Relatório/Plano de Ação - Requisitos Legais', style: 'subheader' },
        { text: '\n'},
        
        this.table(vetor,  ['NR', 'itemNor', 'Descricao', 'Proposta', 'Responsavel', 'Prazo_Execucao', 'Acao_Corretiva', 'Acao_Preventiva', 'Infracao', 'val_Min', 'val_Max', 'Status']),
        { text: '\n'},
      
        { text: 'Total Vl. Max = ' + maxTotal.toFixed(2), style: 'valor' },
        { text: 'Total Vl. Min = ' + minTotal.toFixed(2), style: 'valor' },
        { text: '\n'},
        {
          table: {
            widths: [200, '*', 200, '*'],
            body: [
              ['Parecer Técnico', 'Pontos Críticos'],
              [parTecnico,  parCritico]
            ]
          }
        },
        { text: '\n'},
      ],
      footer: [
        {
          canvas: [
            {
              type: 'line',
              x1: 40,
              y1: 5,
              x2: 800,
              y2: 5,
              lineWidth: 0.5
            }
          ]
        },
        { text: 'www.smsfix.com.br', style: 'footer'},
        { text: 'Tel. (63) 9 8432-5115 | (69) 9 8107-2947 | (69) 9 9987-5884 - E-mail: sac@smsfix.com.br', style: 'footer'},
      ],
      styles: {
        valor: {
          alignment: 'right'
        },
        image: {
          bold: true,
          fontSize: 19,
          alignment: 'right'
        }, 
        nomeDescricao: {
          bold: true,
          fontSize: 18,
          alignment: 'left'
        },
        header: {
          bold: true,
          fontSize: 16,
          alignment: 'left'
        },
        sub_header: {
          fontSize: 16,
          alignment: 'left',
        },
        data: {
          fontSize: 11,
          italic: true,
          alignment: 'right',
          color: '#9E9E9E'
        },
        subheader: {
          old: true,
          fontSize: 20,
          alignment: 'center',
        },
        footer: {
          alignment: 'center',
          color: '#9E9E9E',
          fontSize: 11,
        },
        // telefone: {
        //   alignment: 'center',
        //   color: '#9E9E9E',
        //   fontSize: 12,
        // }
      },
      pageSize: 'A4',
      pageOrientation: 'landscape'
    };

    var dados = pdfmake.createPdf(docDefinition);
    dados.getBuffer(function (buffer) {
      let utf8 = new Uint8Array(buffer);
      let binaryArray = utf8.buffer;
      // self.saveToDevice(binaryArray, minutos, segundos);
      self.abrirPDF(binaryArray, minutos, segundos);
    });
  }
 
  /**
   * MÉTODO PARA CRIAR AS LINHAS E COLUNAS DA TABELA PARA COLOCAR NO PDF
   * @param data 
   * @param columns 
   */
  buildTableBody(data, columns) {
    var body = [];
    body.push(columns);

    data.forEach(function(row) {
      var dataRow = [];
      columns.forEach(function(column) {
        dataRow.push(row[column]);
      })
      body.push(dataRow);
    });
    return body;
  }

  /**
   * MÉTODO PARA CRIAR A TABELA PARA PDF
   * @param data 
   * @param columns 
   */
  table(data, columns) {
    return {
      table: {
        headerRows: 1,
        body: this.buildTableBody(data, columns)
      }
    };
  }

  /**
   * MÉTODO PARA GEAR E SALVAR O ARQUIVO NO DISPOSITIVO E MOSTRAR (REMOVER DEPOIS)
   * @param data 
   * @param savefile 
   */
  saveToDevice(data:any, minutos: any, segundos: any){
    var arquivo: any = "plano_acao"+minutos+segundos+".pdf";

    this.file.createDir(this.file.externalDataDirectory, "smsfix", true)
      .then(link => {
        this.file.writeFile(this.file.externalDataDirectory+'smsfix/plano/', arquivo, data)
        .then(certo => {
          var url = this.file.externalDataDirectory+'smsfix/plano/'+arquivo;
          this.fileOpener.open(url, 'application/pdf')
          .then(certo => {
            console.log('CERTO: ', certo);
          })
          .catch(erro => {
            let toast = this.toastCtrl.create({
              message: 'Problema ao abrir o arquivo.',
              cssClass: 'erro',
              duration: 3000,
              position: 'bottom'
            }).present();
          })
        })
        .catch(erro => {
          this.file.writeExistingFile(this.file.externalDataDirectory+'smsfix/plano/', arquivo, data)
          .then(ok => {
            var url = this.file.externalDataDirectory+'smsfix/plano/'+arquivo;
            this.fileOpener.open(url, 'application/pdf')
            .then(certo => {
              console.log('CERTO: ', certo);
            })
            .catch(erro => {
              let toast = this.toastCtrl.create({
                message: 'Problema ao abrir o arquivo.',
                cssClass: 'erro',
                duration: 3000,
                position: 'bottom'
              }).present();
            })
          })
          .catch(erro => {
            let toast = this.toastCtrl.create({
              message: 'Problema na escrita do arquivo. Tente novamente mais tarde.',
              cssClass: 'erro',
              duration: 3000,
              position: 'bottom'
            }).present();
          })
        })
      });
  }
  
  /**
   * MÉTODO PARA GEAR E SALVAR O ARQUIVO NO DISPOSITIVO E MOSTRAR (REMOVER DEPOIS)
   * @param data 
   * @param minutos 
   * @param segundos 
   */
  abrirPDF(data:any, minutos: any, segundos: any){
    var arquivo: any = "plano_acao"+minutos+segundos+".pdf";
    this.platform.ready()
    .then(() => {
      this.file.createDir(this.file.externalDataDirectory, "smsfix", true)
      .then(link => {
        this.file.writeFile(this.file.externalDataDirectory+'smsfix/', arquivo, data)
          .then(certo => {
            var url = this.file.externalDataDirectory+'smsfix/'+arquivo;
            this.fileOpener.open(url, 'application/pdf')
            .then(certo => {
              console.log('ABRINDO ARQUIVO: ', certo);
            })
            .catch(erro => {
              let toast = this.toastCtrl.create({
                message: 'Você precisa de um aplicativp para abrir arquivo de extensão .pdf.',
                cssClass: 'erro',
                duration: 3000,
                position: 'bottom'
              }).present();
            })
          })
          .catch(erro => {
            let toast = this.toastCtrl.create({
              message: 'Problema na escrita do arquivo. Tente novamente mais tarde.',
              cssClass: 'erro',
              duration: 3000,
              position: 'bottom'
            }).present();
          })
        })
      })
  }

  /**
   * MÉTODO PARA SALVAR E CONVETER O OBJETO JSON EM FORMATO CSV
   * @param jsonValor 
   */
  salvarExcel(jsonValor){
    console.log(jsonValor);
    if(jsonValor != jsonValor.length){
      var minutos = new Date().getMinutes();
      var segundos = new Date().getSeconds();
      let dados = JSON.stringify(jsonValor);
      let sampleJson = JSON.parse(dados);
      let csvData = this.converterForExcel(sampleJson);    
      var arquivo: any = "plano_acao"+minutos+segundos+".csv";

      this.platform.ready()
      .then(() => {
        this.file.createDir(this.file.externalDataDirectory, "smsfix", true)
        .then(link => {
          this.file.writeFile(this.file.externalDataDirectory+'smsfix/', arquivo, csvData)
            .then(certo => {
              var url = this.file.externalDataDirectory+'smsfix/'+arquivo;
              this.fileOpener.open(url, 'text/csv')
              .then(certo => {
                console.log('ABRINDO ARQUIVO: ', certo);
              })
              .catch(erro => {
                let toast = this.toastCtrl.create({
                  message: 'Você precisa de um app para abrir arquivo com extensão .csv.',
                  cssClass: 'erro',
                  duration: 3000,
                  position: 'bottom'
                }).present();
              })
            })
            .catch(erro => {
              let toast = this.toastCtrl.create({
                message: 'Problema na escrita do arquivo. Tente novamente mais tarde.',
                cssClass: 'erro',
                duration: 3000,
                position: 'bottom'
              }).present();
            })
          })
        })
    }
  }
}
