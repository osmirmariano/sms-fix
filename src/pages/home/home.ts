import { StorageProvider } from './../../providers/storage/storage';
import { Component } from '@angular/core';
import { NavController, IonicPage, AlertController, Platform, MenuController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  todos: any;
  public tamanhoBanco: any;
  constructor(
    public navCtrl: NavController,
    public storageProvider: StorageProvider,
    public alertCtrl: AlertController,
    public platform: Platform,
    public menuCtrl: MenuController,
    public serviceProvider: ServiceProvider) {

  }

  /**
   * MÉTODO É ACIONADO SOMENTE QUANDO A PAGE É CARREGADA COMPLETAMENTE, NELE CARREGA MODAL PARA PERGUNTAR 
   * SE TRABALHA EM MODO ONLINE OU OFFLINE.
   */
  ionViewDidLoad(){ 
    // this.storageProvider.tamanho()
    //   .then(tamanho => {
    //     this.tamanhoBanco = tamanho;
    //     console.log('TAMANHO 8888: ', tamanho);
    //   });

    //   let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
    //     console.log('\n\n\nnetwork desconnected!');
    //     if(this.tamanhoBanco == 0){
    //       let alert = this.alertCtrl.create({
    //         title: 'Internet',
    //         subTitle: 'Seu dispositivo não tem acesso a internet, precisamos de acesso para sincronizar alguns dados.',
    //         buttons: ['OK']
    //       });
    //       alert.present();
    //     }
    //   });

    // let connectSubscription = this.network.onConnect().subscribe(() => {
    //   if(this.tamanhoBanco == 0){
    //     console.log('\n\n\nnetwork connected!');
    //     this.alertInternet();
    //   }
    // });
  }

  // alertInternet(){
  //   let confirm = this.alertCtrl.create({
  //     title: 'Método de uso',
  //     message: 'O SMSFIX facilita o uso em modo online ou offline. Escolha a opção',
  //     buttons: [
  //       {
  //         text: 'Online',
  //         handler: () => {
  //           this.modoOnline();
  //         }
  //       },
  //       {
  //         text: 'Offline',
  //         handler: () => {
  //           this.modoOffline();
  //         }
  //       }
  //     ]
  //   });
  //   confirm.present();
  // }

  // modoOnline(){
  //   console.log('TRABALHO MODO ONLINE');
  // }

  // modoOffline(){
  //   console.log('TRABALHO MODO OFFLINE');
  // }


  /**
   * MÉTODO É EXECUTADO QUANDO A PAGE CARREGOU COMPLETAMENTE E AGORA É ATIVA
   */
  ionViewDidEnter(){
    this.menuCtrl.enable(true);
    this.menuCtrl.swipeEnable(true); 
  }

  /**
   * MÉTODO PARA CHAMAR A PAGE DAS NORMAS
   */
  normas(){
    this.navCtrl.push('NormasPage');
  }

  /**
   * MÉTODO QUE RECEBE COMO PARAMETRO UMA URL QUE ACIONA UM PLUGIN PARA ABRIR SITE DO SMSFIX/CONTATOS
   * @param url 
   */
  abrirFaleConosco(url) {
    this.platform.ready()
      .then(() => {
        open(url, "_blank", "location=no");
      });
  }

  /**
   * MÉTODO QUE RECEBE COMO PARAMETRO UMA URL QUE ACIONA UM PLUGIN PARA ABRIR SITE DO MINISTÉRIO 
   * DO TRABALHO
   * @param url 
   */
  abrirCaEpi(url){
    this.platform.ready()
      .then(() => {
        open(url, "_blank", "location=no");
      });
  }

  /**
   * MÉTODO QUE RECEBE COMO PARAMETRO UMA URL QUE ACIONA UM PLUGIN PARA ABRIR SITE DO FACEBBOOK
   * @param url 
   */
  abrirFacebook(url){
    this.platform.ready()
      .then(() => {
        open(url, "_blank", "location=no");
      });
  }

  /**
   * MÉTODO PARA ABRIR A TAB COM OS TIPOS DE AUDITORIAS
   */
  abrirDiagnostico(){
    this.navCtrl.push('AuditoriaTabPage');
  }
}
